package service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import main.MainThread;

public class GeneralQueryService {
	private static EntityManager em = MainThread.em;
	
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> executeQuery(String selectedAttr, String value, String from, Class<T> type){
		Query q = em.createNativeQuery(String.format("SELECT * FROM %2$s a WHERE a.%1s LIKE \'%3$s\'", selectedAttr, from, value), type);
		return q.getResultList();
	}
	
	public static Query find(String namedQuery){
		return em.createNamedQuery(namedQuery);
	}
	
	public static <T> TypedQuery<T> find(String namedQuery, Class<T> type){
		return em.createNamedQuery(namedQuery, type);
	}
}
