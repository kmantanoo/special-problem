package service;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import entity.Song;

public class TableDataService {
	/**
	 * 
	 * @return data data is table information
	 */

	private static void prepairData(final Vector<Vector<String>> data, List<Song> songs){
		if (songs.size() > 0) {
			for (int i = 0; i < songs.size(); i++) {
				Song s = songs.get(i);
				data.add(createVectorRow(i, s));
			}
		} else {
			data.add(createVectorRow());
		}
	}
	
	public static Vector<Vector<String>> loadTableData() {
		Vector<Vector<String>> data = new Vector<>();
		List<Song> songs = SongService.getAllSong();
		prepairData(data, songs);
		return data;
	}
	
	
	
	public static Vector<Vector<String>> loadTableData(List<Song> songList){
		Vector<Vector<String>> data = new Vector<>();
		prepairData(data, songList);
		return data;
	}
	
	private static Vector<String> createVectorRow(){
		String[] row = new String[] { "", "", "", "", "", "", "", "0", "", "" };
		Vector<String> v = new Vector<>();
		v.addAll(Arrays.asList(row));
		return v;
	}
	
	private static Vector<String> createVectorRow(int i, Song s){
		String[] row = new String[] { Integer.toString(i + 1), s.getSongname(),
				s.getCreator().getArtist().getAName(), s.getCreator().getRecord().getRName(),
				s.getSongmood().getMoodLabel(), s.getGenre().getGLabel(), s.getTempo().getTLabel(), Integer.toString(s.getReleaseyear()),
				s.getCreator().getLyricist(), s.getCreator().getMelodywriter() };
		Vector<String> v = new Vector<>();
		v.addAll(Arrays.asList(row));
		return v;
	}

	public static List<String> getColumnNames() {
		return Arrays.asList(new String[] { "ลำดับ", "เพลง",
				"ศิลปิน", "สังกัด",
				"อารมณ์เพลง",
				"แนวเพลง",
				"จังหวะเพลง",
				"ปีที่ปล่อย",
				"ผู้แต่งทำนอง",
				"ผู้แต่งเนื้อร้อง" });
	}
}
