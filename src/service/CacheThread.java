package service;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import entity.Event;

public class CacheThread {
	public static void startCache() {
		TypedQuery<Event> q = Persistence.createEntityManagerFactory("SeniorProject_JPA").createEntityManager().createNamedQuery("Event.findAll", Event.class);
		List<Event> l = q.getResultList();
		Iterator<Event> iter = l.iterator();
		Vector<String> events = new Vector<>();
		while(iter.hasNext()){
			RecommenderService.cacheRecommendFile(iter.next().getEventLabel());
		}
		SongStatusService.setChangeStatus(false);
	}
}
