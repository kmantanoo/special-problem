package service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import main.MainThread;

public class RecommenderService {
	public static final String fileName = "recommend-";
	public static final String fileType = ".rec";
	/***
	 * this method is used to cache recommendation data local file cause the live recommendation is too slow!
	 * @param qValue
	 */
	public static void cacheRecommendFile(String qValue) {
		try {
//			String url = "http://localhost:8080/rec_api/WS/state/reccommend/song/Event";
//			String charset = StandardCharsets.UTF_8.name();
//
//			String query = String.format("event_label=%s", URLEncoder.encode(qValue, charset));
//
//			URLConnection connection = new URL(url + "?" + query).openConnection();
//			connection.setRequestProperty("Accept-Charset", charset);
//			InputStream response = connection.getInputStream();
//
//			Scanner sc = new Scanner(response);
//			String dummyXML = sc.useDelimiter("\\A").next();
//			sc.close();
			String dummyXML = MappingRules.getRecommendXML(qValue);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new ByteArrayInputStream(dummyXML.getBytes(StandardCharsets.UTF_8)));

			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("result");
			PrintWriter fo = getPrintWriter(path()+"recommend-" + qValue + ".rec");
			for(int i = 0; i < nList.getLength(); i++){
				Node n = nList.item(i);
				if(n.getNodeType()==Node.ELEMENT_NODE){
					Element e = (Element) n;
					fo.println(e.getElementsByTagName("song_id").item(0).getTextContent());					
				}
			}
			fo.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	/***
	 * Get printwriter for writer cache data to file
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	private static PrintWriter getPrintWriter(String filename) throws IOException{
		File f = new File(filename);
		if(!f.exists()){
			f.createNewFile();
		}
		PrintWriter pw = new PrintWriter(new FileOutputStream(f, false));
		return pw;
	}

	/***
	 * Get the iterator that iterate over cached file.
	 * @param qValue
	 * @return
	 */
	public static Iterator<Integer> getSIDIter(String qValue){
		String fileName = path() + "recommend-" + qValue + ".rec";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));
			String line;
			ArrayList<Integer> l = new ArrayList<>();
			while((line=reader.readLine())!=null){
				l.add(Integer.parseInt(line));
			}
			reader.close();
			return l.iterator();
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String path(){
		URL url = MainThread.class.getProtectionDomain().getCodeSource().getLocation();
		try {
			String jarPath = new File(url.toURI()).getParentFile().toPath().toString();
			return jarPath + "\\rec_file\\";
		} catch (URISyntaxException e) {
			return "";
		}
	}
}
