package service;

import javax.persistence.EntityManager;

import entity.Admin;

/**
 * Update with comment
 * @author Sololist
 *
 */
public class AdminService {
	public static boolean addAdmin(EntityManager em, String password) {
		try {
			em.getTransaction().begin();

			Admin admin = new Admin();
			admin.setPassword(password);
			em.persist(admin);
			
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean changePassword(EntityManager em, String newPWD) {
		try {
			Admin admin = em.find(Admin.class, 1);
			em.getTransaction().begin();

			admin.setPassword(newPWD);

			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String getPassword(EntityManager em) {
		try {
			Admin admin = em.find(Admin.class, 1);
			return admin.getPassword();
		} catch (Exception e) {
			return null;
		}
	}
}
