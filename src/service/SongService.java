package service;

import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import entity.Creator;
import entity.Genre;
import entity.Song;
import entity.Songmood;
import entity.Tempo;
import main.MainThread;

public class SongService {
	private static EntityManager em = MainThread.em;

	public static boolean newSong(String songName, String artist, String record, String genre, String mood, int year, String tempo,
			String lyricist, String melody) {
		if(hasSong(songName, artist)) return false;
		try {
			CreatorService.addCreator(artist, record, lyricist, melody);
			Creator c = CreatorService.findCreator(artist, record, lyricist, melody);
			
			
			Song s = new Song();
			s.setSongname(songName);
			
			s.setGenre(GeneralQueryService.executeQuery("g_label", genre, "Genre", Genre.class).get(0));
			s.setSongmood(GeneralQueryService.executeQuery("mood_label", mood, "Songmood", Songmood.class).get(0));
			s.setReleaseyear(year);
			s.setTempo(GeneralQueryService.executeQuery("t_label", tempo, "Tempo", Tempo.class).get(0));
			s.setCreator(c);
			
			em.getTransaction().begin();
			em.persist(s);
			em.getTransaction().commit();
			SongStatusService.setChangeStatus(true);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean hasSong(String songname, String artistname){
		String sql = "SELECT s.sid FROM Song s WHERE s.songname=:song and s.cid.aid.aName=:artist";
		Query q = em.createQuery(sql);
		q.setParameter("song", songname);
		q.setParameter("artist", artistname);
		return q.getResultList().size()>0;
	}

	public static int getSongCount() {
		Query q = em.createQuery("SELECT COUNT(s) FROM Song s");
		return Integer.parseInt(q.getResultList().get(0).toString());
	}

	public static int getMoodCount() {
		Query q = em.createQuery("SELECT COUNT(DISTINCT(s.songmood)) FROM Song s");
		return Integer.parseInt(q.getResultList().get(0).toString());
	}
	
	public static List<Song> getAllSong(){
		TypedQuery<Song> q = GeneralQueryService.find("Song.findAll", Song.class);
		return q.getResultList();
	}
	
	public static boolean deleteSong(String songname, String artistname, int releaseyear){
		try {
			em.getTransaction().begin();
			em.createQuery("DELETE FROM Song s WHERE s.songname=?1 and s.cid.aid.aName=?2 and s.releaseyear=?3")
			.setParameter(1, songname)
			.setParameter(2, artistname)
			.setParameter(3, releaseyear)
			.executeUpdate();
			
			
			em.getTransaction().commit();
			SongStatusService.setChangeStatus(true);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static Song findSong(String songname, String artistname, int releaseyear){
		try {
			Query q = em.createQuery("SELECT s FROM Song s WHERE s.songname=?1 and s.cid.aid.aName=?2 and s.releaseyear=?3");
			q.setParameter(1, songname);
			q.setParameter(2, artistname);
			q.setParameter(3, releaseyear);
			return (Song) q.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public static void updateSong(Song s, Vector<String> songInfo) {
		String songName, artistName, lyricist, melodyWriter, tempo, genre, songMood, recordName;
		int year;
		
		songName = songInfo.get(0);
		artistName = songInfo.get(1);
		recordName = songInfo.get(2);
		genre = songInfo.get(3);
		songMood = songInfo.get(4);
		tempo = songInfo.get(6);
		lyricist = songInfo.get(7);
		melodyWriter = songInfo.get(8);
		
		year = Integer.parseInt(songInfo.get(5));	
		
		CreatorService.addCreator(artistName, recordName, lyricist, melodyWriter);
		Creator c = CreatorService.findCreator(artistName, recordName, lyricist, melodyWriter);
		Tempo t = em.createNamedQuery("Tempo.findIDByLabel", Tempo.class).setParameter("tempo_label", tempo).getSingleResult();
		Genre g = em.createNamedQuery("Genre.findIDByLabel", Genre.class).setParameter("genre_label", genre).getSingleResult();
		Songmood sm = em.createNamedQuery("Songmood.findIDByLabel", Songmood.class).setParameter("song_mood_label", songMood).getSingleResult();
		
		Song found = em.find(Song.class,s.getSid());
		found.setSongname(songName);
		found.setCreator(c);
		found.setTempo(t);
		found.setGenre(g);
		found.setSongmood(sm);
		found.setReleaseyear(year);
		em.merge(found);
	}

}
