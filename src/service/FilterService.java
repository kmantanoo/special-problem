package service;

import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import expert.ExpertMainWindow;
import mainsearch.MainSearchMainWindow;

public class FilterService {
	private DefaultTableModel searchedTableData;
	private Pattern p;
	private Vector<Vector<String>> initialTableData;
	private JTable table;
	private JPanel pnlInstance;
	private JFrame mainWindow;
	
	
	
	public FilterService(Vector<Vector<String>> initialTableData, JTable table, JPanel pnlInstance, JFrame mainWindow) {
		super();
		this.initialTableData = initialTableData;
		this.table = table;
		this.pnlInstance = pnlInstance;
		this.mainWindow = mainWindow;
	}

	@SuppressWarnings({ "unchecked" })
	public void applyFilter(HashMap<String, String> filter){
		
		Vector<Vector<String>> modelVector = null;
		if(searchedTableData == null){
			modelVector = initialTableData;
		}else {
			modelVector = ((DefaultTableModel) table.getModel()).getDataVector();
		}
		Vector<Vector<String>> output = new Vector<>();
		for(Vector<String> row:modelVector){
			String artist, record, genre, tempo;
			int releaseYear, from, to;
			artist = row.get(2);
			record = row.get(3);
			genre = row.get(5);
			tempo = row.get(6);
			releaseYear = Integer.parseInt(row.get(7));
			from = Integer.parseInt(filter.get("from"));
			to = Integer.parseInt(filter.get("to"));
			
			
			if(matchArtist(filter.get("artist"), artist) && 
					matchRecord(filter.get("record"), record) &&
					matchGenre(filter.get("genre"), genre) &&
					matchTempo(filter.get("tempo"), tempo) &&
					(releaseYear >= from && releaseYear <= to)){
				output.add(row);
			}
		}
		
		updateTable(output, false, 2);
		
	}
	
	private boolean matchGenre(String filter, String genre) {
		if(!filter.equals("------")){
			p = Pattern.compile(filter);
			Matcher m = p.matcher(genre);
			return m.matches();
		}
		return true;
	}

	private boolean matchArtist(String filter, String value){
		if(!filter.equals("------")){
			p = Pattern.compile(filter);
			Matcher m = p.matcher(value);
			return m.matches();
		}
		return true;
	}
	
	private boolean matchRecord(String filter, String value){
		if(!filter.equals("------")){
			p = Pattern.compile(filter);
			Matcher m = p.matcher(value);
			return m.matches();
		}
		return true;
	}
	
	private boolean matchTempo(String filter, String value){
		if(!filter.equals("------")){
			p = Pattern.compile(filter);
			Matcher m = p.matcher(value);
			return m.matches();
		}
		return true;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void resetFilter(){
		Vector<Vector<String>> modelVector = ((DefaultTableModel)table.getModel()).getDataVector();
		updateTable(modelVector, true, 3);
		updateFilter();
	}
	
	public void updateTable(Vector<Vector<String>> rowData, boolean filterUpdate, int callerStatus){
		/***
		 * callerStatus 1 for searchButton, 2 for applyFilter method and 3 for resetFilter method.
		 */
		DefaultTableModel model = new DefaultTableModel(rowData, new Vector<>(TableDataService.getColumnNames()));
		table.setModel(model);
		if(callerStatus==1){
			searchedTableData = model;
		}
		if(filterUpdate) updateFilter();
		pnlInstance.invalidate();
	}
	
	public void resetTable(int callerStatus){
		/***
		 * callerStatus 1 for clear searched table and 2 for unused filter. 3 for reload data from db.
		 */
		if(callerStatus==1 || (callerStatus==2 && searchedTableData == null)){
			DefaultTableModel model = new DefaultTableModel(initialTableData, new Vector<>(TableDataService.getColumnNames()));
			table.setModel(model);
			searchedTableData=null;
		} else if(callerStatus == 2 && searchedTableData != null){
			table.setModel(searchedTableData);
		} else if(callerStatus == 3){
			Vector<Vector<String>> v = TableDataService.loadTableData();
			DefaultTableModel model = new DefaultTableModel(v, new Vector<>(TableDataService.getColumnNames()));
			table.setModel(model);
		}
		pnlInstance.invalidate();
	}
	
	public void updateFilter(){
		if(mainWindow instanceof MainSearchMainWindow) {
			((MainSearchMainWindow) mainWindow).updateFilter(table);
		} else {
			((ExpertMainWindow) mainWindow).updateFilter(table);
		}
	}

}
