package service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Vector;

import javax.persistence.TypedQuery;

import entity.Event;

public class FileService {
	public static void copyFile(File src, File dest) throws IOException {
		Files.copy(src.toPath(), dest.toPath());
	}

	public static void createAllCachedTemp() {
		String leader = RecommenderService.fileName;
		String type = RecommenderService.fileType;
		String path = RecommenderService.path();
		
		ArrayList<File> inputFiles = new ArrayList<File>();
		ArrayList<File> outputFiles = new ArrayList<>();
		TypedQuery<Event> events = GeneralQueryService.find("Event.findAll", Event.class);
		Vector<Event> ev = new Vector<>(events.getResultList());
		for(Event e:ev){
			inputFiles.add(new File(path+leader+e.getEventLabel()+type));
			outputFiles.add(new File(path+"temp-"+leader+e.getEventLabel()+type));
		}
		for(int i = 0 ; i < inputFiles.size() ; i++){
			try {
				copyFile(inputFiles.get(i), outputFiles.get(i));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public static void clearCacheTemp(){
		String leader = RecommenderService.fileName;
		String type = RecommenderService.fileType;
		String path = RecommenderService.path();

		ArrayList<File> inputFiles = new ArrayList<>();
		TypedQuery<Event> events = GeneralQueryService.find("Event.findAll", Event.class);
		Vector<Event> ev = new Vector<>(events.getResultList());
		for(Event e:ev){
			inputFiles.add(new File(path+"temp-"+leader+e.getEventLabel()+type));
		}
		for(int i = 0 ; i < inputFiles.size() ; i++){
			try {
				Files.delete(inputFiles.get(i).toPath());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
