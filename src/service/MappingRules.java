package service;

import java.util.HashMap;
import java.util.List;

import javax.persistence.TypedQuery;

import entity.Song;
import entity.Songmood;

public class MappingRules {
	public static String[] wedding = new String[]{"คิดถึง","แอบรัก", "เหงา", "รักซึ้งๆ", "บอกรัก", "อ้อน", "ง้อ"};
	public static String[] reunion = new String[]{"เพื่อน","ให้กำลังใจ", "คิดถึง", "รักซึ้ง", "บอกรัก"};
	public static String[] goodbye = new String[]{"เพื่อน","ให้กำลังใจ", "เหงา", "รักซึ้งๆ", "อ้อน","ง้อ"};
	public static String[] bday = new String[]{"ให้กำลังใจ", "บอกรัก"};
	public static String[] festival = new String[]{"เพื่อน","ให้กำลังใจ","คิดถึง","รักซึ้ง", "บอกรัก", "ตัดพ้อ"};
	public static String[] brokenheart = new String[]{"คิดถึง","เหงา", "แอบรัก","เหงา","แฟนเก่า", "อกหัก", "ตัดพ้อ", "อ้อน", "ง้อ"};
	
	public static HashMap<String, String[]> getMappingRules(){
		HashMap<String, String[]> mRules = new HashMap<String, String[]>();
		mRules.put("งานแต่งงาน", wedding);
		mRules.put("งานเลี้ยงรุ่น", reunion);
		mRules.put("งานเลี้ยงอำลา", goodbye);
		mRules.put("งานวันเกิด", bday);
		mRules.put("งานเทศกาล", festival);
		mRules.put("คนอกอัก", brokenheart);
		return mRules;
	}
	
	public static String getRecommendXML(String event){
		String xmlHeader = "<?xml version=\"1.0\"?><results>";
		String resultTail = "</results>";
		String output = "";
		for(String moods:MappingRules.getMappingRules().get(event)){
			Songmood mood = GeneralQueryService.find("Songmood.findIDByLabel", Songmood.class).setParameter("song_mood_label", moods).getSingleResult();
			List<Song> results = GeneralQueryService.find("Song.findBySongmood", Song.class).setParameter("moodid", mood).getResultList();
			for(Song s:results){
				output += "<result><song_id>" + s.getSid() + "</song_id><result>";
			}
		}
		return xmlHeader + output + resultTail;
	}
	
	public static void main(String[] args) {
		System.out.println(getRecommendXML("งานแต่งงาน"));
	}
}
