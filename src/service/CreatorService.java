package service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entity.Artist;
import entity.Creator;
import entity.Record;
import main.MainThread;

public class CreatorService {
	private static EntityManager em = MainThread.em;
	
	public static boolean addCreator(String artist, String record, String lyricist, String melodyWriter){
		try {
			if(findCreator(artist, record, lyricist, melodyWriter) == null){
				
				if(ArtistService.findArtistByName(artist)==null){
					ArtistService.createArtist(artist);
				}
				if(RecordService.findRecordByName(record)==null){
					RecordService.createRecord(record);
				}
				
				Artist a = ArtistService.findArtistByName(artist);
				Record r = RecordService.findRecordByName(record);
				
				
				em.getTransaction().begin();
				Creator c = new Creator();
				c.setArtist(a);
				c.setRecord(r);
				c.setLyricist(lyricist);
				c.setMelodywriter(melodyWriter);
				
				em.persist(c);
				em.getTransaction().commit();
				
				System.out.println("Add creator complete");
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static Creator findCreator(String artist, String record, String lyricist, String melodyWriter){
		String sql = "SELECT c FROM Creator c WHERE c.aid.aName=:artist and c.rid.rName=:record and c.lyricist=:lyric and c.melodywriter=:melody";
		Query q = em.createQuery(sql);
		 q.setParameter("artist", artist);
		 q.setParameter("record", record);
		 q.setParameter("lyric", lyricist);
		 q.setParameter("melody", melodyWriter);
		 
		@SuppressWarnings("unchecked")
		List<Creator> a = q.getResultList();
		System.out.println(a);
		if(q.getResultList().size()>0) return (Creator) q.getSingleResult();
		return null;
	}
}
