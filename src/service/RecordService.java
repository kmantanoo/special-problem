package service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entity.Record;
import main.MainThread;

public class RecordService {
	private static EntityManager em = MainThread.em;
	public static boolean createRecord(String recordName) {
		try {
			em.getTransaction().begin();

			Record r = new Record();
			r.setRName(recordName);

			em.persist(r);
			em.getTransaction().commit();
			
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Record findRecord(int rid) {
		return em.find(Record.class, rid);
	}
	
	public static Record findRecordByName(String rname){
		Query q = em.createQuery("SELECT r.rId FROM Record r WHERE r.rName=:rname").setParameter("rname", rname);
		int id = Integer.parseInt(q.getResultList().get(0).toString());
		return findRecord(id);
	}
	
	public static boolean updateRecord(int rid, String newName){
		Record r = findRecord(rid);
		if(r==null) return false;
		em.getTransaction().begin();
		
		r.setRName(newName);
		
		em.getTransaction().commit();
		
		return true;
	}
	
	public static boolean deleteRecord(int rid){
		if(findRecord(rid) == null) return false;
		em.getTransaction().begin();
		em.remove(findRecord(rid));
		em.getTransaction().commit();
		
		return true;
		
		
	}

	public static Record findArtistByName(String recordName) {
		Query q = em.createQuery("SELECT r.rid FROM Record r WHERE r.recordName LIKE '" + recordName + "'");
		int id = Integer.parseInt(q.getResultList().get(0).toString());
		return findRecord(id);
	}

	@SuppressWarnings("unchecked")
	public static List<String> getAllRecordName() {
		String sql = "SELECT r.rName FROM Record r";
		Query q = em.createQuery(sql);
		return (List<String>) q.getResultList();
	}

	public static int getRecordCount() {
		Query q = em.createQuery("SELECT COUNT(r) FROM Record r");
		return Integer.parseInt(q.getResultList().get(0).toString());
	}
}
