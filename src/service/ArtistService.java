package service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import entity.Artist;
import main.MainThread;

public class ArtistService {
	private static EntityManager em = MainThread.em;
	public static boolean createArtist(String artistName) {
		try {
			em.getTransaction().begin();

			Artist artist = new Artist();
			artist.setAName(artistName);

			em.persist(artist);
			em.getTransaction().commit();

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Artist findArtist(int aid) {
		return em.find(Artist.class, aid);
	}
	
	public static Artist findArtistByName(String aname){
		Query q = em.createQuery("SELECT a.aId FROM Artist a WHERE a.aName=:aname");
		q.setParameter("aname", aname);
		int id = Integer.parseInt(q.getResultList().get(0).toString());
		return findArtist(id);
	}

	public static boolean updateArtist(int aid, String artistName) {
		Artist artist = findArtist(aid);
		if (artist != null) {
			em.getTransaction().begin();
			artist.setAName(artistName);
			em.getTransaction().commit();
			return true;
		} else {
			return false;
		}
	}

	public static boolean deleteArtist(int aid) {
		Artist artist = findArtist(aid);
		if (artist != null) {
			em.getTransaction().begin();
			em.remove(artist);
			em.getTransaction().commit();
			return true;
		} else {
			return false;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static List getAllArtist() {
		String sql = "SELECT * FROM Artist";
		Query q = em.createQuery(sql);
		return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> getAllArtistName(){
		String sql = "SELECT a.aName FROM Artist a";
		Query q = em.createQuery(sql);
		return (List<String>) q.getResultList();
	}

	public static int getArtistCount() {
		Query q = em.createQuery("SELECT COUNT(a) FROM Artist a");
		return Integer.parseInt(q.getResultList().get(0).toString());
	}
}
