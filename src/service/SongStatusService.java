package service;

import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import main.MainThread;

public class SongStatusService {
	private static final EntityManager em = MainThread.em;
	
	public static boolean getChangeStatus(){
		Query q = em.createNativeQuery("SELECT has_change FROM SONG_STATUS WHERE status_id=1");
		@SuppressWarnings("unchecked")
		Vector<String> results = (Vector<String>) q.getResultList();
		return Boolean.parseBoolean(results.get(0).toString());
	}
	
	public static void setChangeStatus(boolean hasChange){
		em.getTransaction().begin();
		em.createNativeQuery("UPDATE SONG_STATUS SET has_change=?1 WHERE status_id=1").setParameter(1, Boolean.toString(hasChange)).executeUpdate();
		em.getTransaction().commit();
	}
	
	public static void main(String[] args) {
		System.out.println(getChangeStatus());
	}
}
