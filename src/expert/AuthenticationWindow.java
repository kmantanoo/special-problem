package expert;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import main.MainThread;
import service.AdminService;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;

public class AuthenticationWindow extends JFrame implements WindowListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static AuthenticationWindow instance;
	private JPasswordField textField;

	
	
	/**
	 * Instantiate the instance of Authentication Window
	 */
	public static void instantiate(){
		if(instance==null) instance = new AuthenticationWindow();
	}

	/**
	 * Create the application.
	 */
	private AuthenticationWindow() {
		initialize();
		addWindowListener(this);
		setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Authentication");
		setBounds(550, 100, 450, 200);
		getContentPane().setLayout(null);
		
		
		ImageIcon icon = new ImageIcon(getClass().getResource("/authentication_image.png"));
		
		JLabel lblNewLabel = new JLabel(icon);
		lblNewLabel.setBounds(10, 11, 128, 128);
		getContentPane().add(lblNewLabel);
		
		textField = new JPasswordField();
		textField.setBounds(159, 65, 166, 20);
		getContentPane().add(textField);
		textField.setColumns(20);
		
		ImageIcon buttonIcon = new ImageIcon(getClass().getResource("/verified_expert_user.png"));
		JButton btnVerify = new JButton(buttonIcon);
		btnVerify.setBounds(159, 96, 48, 48);
		btnVerify.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent ae){
				String txtPassword, dbPassword;
				txtPassword = String.copyValueOf(textField.getPassword());
				dbPassword = AdminService.getPassword(MainThread.em);
				
				if(txtPassword.compareTo(dbPassword)==0){
					// If passed verification step
					int select = JOptionPane.showConfirmDialog(null, "Succesfully", "Login OK", JOptionPane.OK_CANCEL_OPTION);
					if(select == JOptionPane.OK_OPTION) {
						dispose();
						ExpertMainWindow.instantiate();
					}
				} else {
					JOptionPane.showMessageDialog(null, "Incorrect Password");
					textField.setText("");
				}
				clearInstance();
//				ExpertMainWindow.instantiate();
			}

		});
		getRootPane().setDefaultButton(btnVerify);
		getContentPane().add(btnVerify);
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setFont(new Font("Arial", Font.PLAIN, 18));
		lblNewLabel_1.setBounds(160, 32, 130, 22);
		getContentPane().add(lblNewLabel_1);
		
	}
	
	public static void clearInstance() {
		instance = null;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		clearInstance();
		dispose();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
