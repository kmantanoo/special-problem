package expert;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import entity.Artist;
import main.MainThread;
import service.ArtistService;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class ArtistManageWindow extends JFrame implements WindowListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ArtistManageWindow instance;
	private static JFrame callback;
	private JComboBox<String> comboArtist;

	/**
	 * Create the panel.
	 */
	private ArtistManageWindow() {
		getContentPane().setLayout(null);
		initialize();
		setVisible(true);
	}

	private void initialize() {
		setSize(330, 130);
		setTitle("จัดการศิลปิน");
		JLabel lblNewLabel = new JLabel("ศิลปิน");
		lblNewLabel.setBounds(10, 11, 46, 14);
		getContentPane().add(lblNewLabel);

		comboArtist = new JComboBox<>();
		comboArtist.setBounds(66, 8, 231, 20);
		loadModel();
		getContentPane().add(comboArtist);

		JButton btnAdd = new JButton("เพิ่ม");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddArtistWindow.instantiate(ArtistManageWindow.this);
			}
		});
		btnAdd.setBounds(10, 55, 89, 23);
		getContentPane().add(btnAdd);

		JButton btnMod = new JButton("แก้ไข");
		btnMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Artist selected = ArtistService.findArtistByName(comboArtist.getSelectedItem().toString());
				EditArtistWindow.instantiate(ArtistManageWindow.this, selected, comboArtist.getSelectedIndex());
			}
		});
		btnMod.setBounds(109, 55, 89, 23);
		getContentPane().add(btnMod);

		JButton btnDel = new JButton("ลบ");
		btnDel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Artist selected = ArtistService.findArtistByName(comboArtist.getSelectedItem().toString());
				int optionSelected = JOptionPane.showConfirmDialog(btnDel,
						"ต้องการลบ  " + selected.getAName() + " หรือไม่", "ลบศิลปิน", JOptionPane.OK_CANCEL_OPTION);
				if (optionSelected == JOptionPane.OK_OPTION) {
					try {
						MainThread.em.getTransaction().begin();
						MainThread.em.remove(selected);
						MainThread.em.getTransaction().commit();
						JOptionPane.showMessageDialog(btnDel, "ลบศิลปินสำเร็จ");
						loadModel();
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(btnDel, "ไม่สามารถลบศิลปินได้");
					}
				}
			}
		});
		btnDel.setBounds(208, 55, 89, 23);
		getContentPane().add(btnDel);
		addWindowListener(this);
	}

	public void loadModel() {
		List<String> result = ArtistService.getAllArtistName();
		Vector<String> v = new Vector<String>(result);
		ComboBoxModel<String> model = new DefaultComboBoxModel<>(v);
		setArtistComboModel(model);
	}

	public void loadModel(int selectedRow) {
		loadModel();
		comboArtist.setSelectedIndex(selectedRow);
		invalidate();
	}

	public static void instantiate(JFrame c) {
		if (instance == null) {
			instance = new ArtistManageWindow();
			callback = c;
		}

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void setArtistComboModel(ComboBoxModel<String> model) {
		comboArtist.setModel(model);
		invalidate();
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	private void closing() {
		callback = null;
		instance = null;
		dispose();

	}

	@Override
	public void windowClosing(WindowEvent e) {
		if(callback instanceof InsertWindows){
			((InsertWindows)callback).invalidateArtist();
		} else if(callback instanceof EditWindow){
			((EditWindow) callback).invalidateArtist();
		}
		closing();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}
}
