package expert;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import service.ArtistService;
import javax.swing.JButton;

public class AddArtistWindow extends JFrame implements WindowListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static AddArtistWindow instance;
	private JTextField textField;
	private static ArtistManageWindow callback;


	/**
	 * Create the application.
	 */
	private AddArtistWindow() {
		initialize();
		setVisible(true);
	}
	
	public static void instantiate(ArtistManageWindow c) {
		callback = c;
		if(instance == null) instance = new AddArtistWindow();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(200, 100, 347, 74);
		setTitle("เพิ่มศิลปิน");
		getContentPane().setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
		
		JLabel lblNewLabel = new JLabel("ศิลปิน");
		getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		getContentPane().add(textField);
		textField.setColumns(20);
		
		JButton btnAddArtist = new JButton("เพิ่ม");
		getContentPane().add(btnAddArtist);
		btnAddArtist.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String artistName = textField.getText();
				if(artistName.compareTo("")==0){
					JOptionPane.showMessageDialog(btnAddArtist, "โปรดใส่ชื่อของศิลปิน");
				} else {
					if(ArtistService.createArtist(artistName)) {
						JOptionPane.showMessageDialog(btnAddArtist, "เพิ่มศิลปินสำเร็จ");
						closing();
						callback.loadModel();
					} else {
						JOptionPane.showMessageDialog(btnAddArtist, "ไม่สามารถเพิ่มศิลปินได้");
					}
				}
			}
		});
		
		JButton btnCancel = new JButton("ยกเลิก");
		getContentPane().add(btnCancel);
		btnCancel.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				closing();
				System.out.println(instance);
			}
		});
		
		addWindowListener(this);
	}
	
	private void closing(){
		dispose();
		instance = null;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		closing();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
