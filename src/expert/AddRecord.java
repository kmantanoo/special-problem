package expert;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import service.RecordService;

import javax.swing.JButton;

public class AddRecord extends JFrame implements WindowListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static AddRecord instance;
	private static RecordManageWindow callback;
	private JTextField textField;

	/**
	 * Create the application.
	 */
	private AddRecord() {
		initialize();
		setVisible(true);
	}

	public static void instantiate(RecordManageWindow r) {
		if (instance == null){
			instance = new AddRecord();
			callback = r;
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(200, 100, 313, 74);
		setTitle("เพิ่มสังกัด");
		getContentPane().setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

		JLabel lblNewLabel = new JLabel("สังกัด");
		getContentPane().add(lblNewLabel);

		textField = new JTextField();
		getContentPane().add(textField);
		textField.setColumns(15);

		JButton btnAddRecord = new JButton("เพิ่ม");
		getContentPane().add(btnAddRecord);
		btnAddRecord.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String recordName = textField.getText();
				if (RecordService.createRecord(recordName)) {
					JOptionPane.showMessageDialog(btnAddRecord, "เพิ่มสังกัดสำเร็จ");
					closing();
					callback.loadModel();
				} else {
					JOptionPane.showMessageDialog(btnAddRecord, "ไม่สามารถเพิ่มสังกัดได้");
				}
			}
		});

		JButton btnCancel = new JButton("ยกเลิก");
		getContentPane().add(btnCancel);
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closing();
			}
		});
		addWindowListener(this);
	}

	private void closing() {
		dispose();
		instance = null;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		closing();

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

}
