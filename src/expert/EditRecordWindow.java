package expert;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import entity.Record;
import main.MainThread;
import javax.persistence.EntityManager;
import javax.swing.JButton;

public class EditRecordWindow extends JFrame implements WindowListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static EditRecordWindow instance;
	private static int recordIndex;
	private static Record record;
	private JTextField textField;
	private static RecordManageWindow callback;

	/**
	 * Create the application.
	 */
	private EditRecordWindow() {
		initialize();
		setVisible(true);
	}

	public static void instantiate(RecordManageWindow r, Record selected, int index) {
		recordIndex = index;
		EditRecordWindow.record = selected;
		callback = r;
		if (instance == null)
			instance = new EditRecordWindow();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setBounds(200, 100, 360, 74);
		setTitle("แก้ไขศิลปิน");
		getContentPane().setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

		JLabel lblNewLabel = new JLabel("ศิลปิน");
		getContentPane().add(lblNewLabel);

		textField = new JTextField(record.getRName());
		textField.select(0, record.getRName().length());
		getContentPane().add(textField);
		textField.setColumns(20);

		JButton btnEditArtist = new JButton("แก้ไข");
		getContentPane().add(btnEditArtist);
		btnEditArtist.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String recordName = textField.getText();
				EntityManager em = MainThread.em;
				if (recordName.compareTo("") == 0) {
					JOptionPane.showMessageDialog(btnEditArtist, "โปรดใส่ชื่อของศิลปิน");
				} else {
					try {
						em.getTransaction().begin();
						record.setRName(recordName);
						em.getTransaction().commit();
						JOptionPane.showMessageDialog(btnEditArtist, "แก้ไขศิลปินสำเร็จ");
						closing();
						callback.loadModel(recordIndex);
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(btnEditArtist, "ไม่สามารถแก้ไขศิลปิน");
					}
				}
			}
		});

		JButton btnCancel = new JButton("ยกเลิก");
		getContentPane().add(btnCancel);
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closing();
			}
		});

		addWindowListener(this);
	}

	private void closing() {
		dispose();
		instance = null;
		record = null;
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		closing();
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

}
