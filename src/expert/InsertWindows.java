package expert;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import entity.Artist;
import entity.Genre;
import entity.Record;
import entity.Songmood;
import entity.Tempo;
import service.GeneralQueryService;
import service.SongService;
import javax.swing.JTextField;
import javax.persistence.TypedQuery;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JSpinner;

public class InsertWindows extends JFrame implements WindowListener, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static InsertWindows instance;
	private JComboBox<Object> cbArtist, cbRecord, cbTempo, cbGenre, cbMood;
	private ButtonGroup bg;
	private JTextField tfFileLocation;
	private JRadioButton radDetail, radFromFile;
	private JPanel panel_1;
	private JPanel panel;
	private JTextField tfSongName;
	private JTextField tfLyrics;
	private JTextField tfMelody;
	private JSpinner spYear;
	private static ExpertMainSearchPanel callback;

	public static void instantiate(ExpertMainSearchPanel expertMainSearchPanel) {
		callback = expertMainSearchPanel;
		if (instance == null)
			instance = new InsertWindows();
	}

	/**
	 * Create the application.
	 */
	public InsertWindows() {
		initialize();
		addWindowListener(this);
		disableDetail();
		setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() {
		bg = new ButtonGroup();

		setTitle("เพิ่มข้อมูลเพลง");
		setBounds(100, 100, 522, 457);
		getContentPane().setLayout(null);

		radDetail = new JRadioButton(
				"เพิ่มจากข้อมูล");
		radDetail.setBounds(127, 7, 111, 23);
		getContentPane().add(radDetail);

		radFromFile = new JRadioButton(
				"เพิ่มจากไฟล์ CSV");
		radFromFile.setBounds(10, 7, 115, 23);
		getContentPane().add(radFromFile);

		bg.add(radFromFile);
		bg.add(radDetail);
		radFromFile.setSelected(true);
		radFromFile.addActionListener(this);
		radDetail.addActionListener(this);

		panel = new JPanel();
		panel.setName("file");
		TitledBorder titleOne = new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		titleOne.setTitle("From file");
		panel.setBorder(titleOne);
		panel.setBounds(10, 37, 478, 62);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("ไฟล์");
		lblNewLabel.setBounds(10, 32, 21, 14);
		panel.add(lblNewLabel);

		tfFileLocation = new JTextField();
		tfFileLocation.setToolTipText("<html>Location of CSV file<br>Format:<br>SONG_NAME,ARTIST_NAME,RECORD_NAME,GENRE,SONG_MOOD,RELEASE_YEAR,TEMPO,LYRICIST,MELODY_WRITER</html>");
		tfFileLocation.setBounds(30, 29, 354, 20);
		panel.add(tfFileLocation);
		tfFileLocation.setColumns(10);

		JButton btnChoose = new JButton("เลือกไฟล์");
		btnChoose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma Separated Value (*.csv)", "csv");
				JFileChooser fileChooser = new JFileChooser(".");
				fileChooser.setFileFilter(filter);
				int ret = fileChooser.showOpenDialog(InsertWindows.this);
				if(ret == JFileChooser.APPROVE_OPTION){
					tfFileLocation.setText(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnChoose.setBounds(389, 28, 79, 23);
		panel.add(btnChoose);

		panel_1 = new JPanel();
		panel_1.setName("detail");
		TitledBorder titleTwo = new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		titleTwo.setTitle("Detail");
		panel_1.setBorder(titleTwo);
		panel_1.setBounds(10, 110, 478, 261);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("เพลง");
		lblNewLabel_1.setBounds(10, 14, 46, 14);
		panel_1.add(lblNewLabel_1);

		tfSongName = new JTextField();
		tfSongName.setBounds(109, 11, 173, 20);
		panel_1.add(tfSongName);
		tfSongName.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("ศิลปิน");
		lblNewLabel_2.setBounds(10, 39, 46, 14);
		panel_1.add(lblNewLabel_2);

		cbArtist = new JComboBox<>();
		cbArtist.setBounds(109, 36, 144, 20);
		invalidateArtist();
		panel_1.add(cbArtist);

		JButton btnNewArtist = new JButton("จัดการศิลปิน");
		btnNewArtist.setBounds(366, 32, 102, 23);
		panel_1.add(btnNewArtist);
		btnNewArtist.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArtistManageWindow.instantiate(InsertWindows.this);
			}
		});

		JLabel lblNewLabel_3 = new JLabel("สังกัด");
		lblNewLabel_3.setBounds(10, 64, 46, 14);
		panel_1.add(lblNewLabel_3);

		cbRecord = new JComboBox<>();
		cbRecord.setBounds(109, 61, 144, 20);
		invalidateRecord();
		panel_1.add(cbRecord);

		JLabel lblNewLabel_4 = new JLabel("ปีที่ปล่อย");
		lblNewLabel_4.setBounds(10, 149, 46, 14);
		panel_1.add(lblNewLabel_4);

		spYear = new JSpinner();
		spYear.setBounds(109, 146, 63, 20);
		spYear.setEditor(new JSpinner.NumberEditor(spYear, "#"));
		spYear.setValue(Calendar.getInstance().get(Calendar.YEAR)+543);
		panel_1.add(spYear);

		JButton btnNewRecord = new JButton("จัดการสังกัด");
		btnNewRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RecordManageWindow.instantiate(InsertWindows.this);
			}
		});
		btnNewRecord.setBounds(366, 57, 102, 23);
		panel_1.add(btnNewRecord);

		JLabel lblNewLabel_5 = new JLabel(
				"ผู้ประพันธ์เนื้อร้อง");
		lblNewLabel_5.setBounds(10, 208, 79, 14);
		panel_1.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel(
				"ผู้ประพันธ์ทำนอง");
		lblNewLabel_6.setBounds(10, 233, 79, 14);
		panel_1.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("แนวเพลง");
		lblNewLabel_7.setBounds(10, 90, 46, 14);
		panel_1.add(lblNewLabel_7);

		TypedQuery<Genre> genre = GeneralQueryService.find("Genre.findAll", Genre.class);
		cbGenre = new JComboBox<>();
		cbGenre.setBounds(109, 87, 79, 20);
		cbGenre.setModel(new DefaultComboBoxModel<>(new Vector<>(genre.getResultList())));
		panel_1.add(cbGenre);

		tfLyrics = new JTextField();
		tfLyrics.setBounds(109, 205, 144, 20);
		panel_1.add(tfLyrics);
		tfLyrics.setColumns(10);

		tfMelody = new JTextField();
		tfMelody.setBounds(109, 230, 144, 20);
		panel_1.add(tfMelody);
		tfMelody.setColumns(10);

		JLabel lblNewLabel_8 = new JLabel("จังหวะเพลง");
		lblNewLabel_8.setBounds(10, 180, 63, 14);
		panel_1.add(lblNewLabel_8);

		TypedQuery<Tempo> tempo = GeneralQueryService.find("Tempo.findAll", Tempo.class);
		cbTempo = new JComboBox<>();
		cbTempo.setBounds(109, 177, 79, 20);
		cbTempo.setModel(new DefaultComboBoxModel<>(new Vector<>(tempo.getResultList())));
		panel_1.add(cbTempo);

		JLabel lblNewLabel_9 = new JLabel("อารมณ์เพลง");
		lblNewLabel_9.setBounds(10, 121, 63, 14);
		panel_1.add(lblNewLabel_9);
		
		TypedQuery<Songmood> songmood = GeneralQueryService.find("Songmood.findAll", Songmood.class);
		cbMood = new JComboBox();
		cbMood.setModel(new DefaultComboBoxModel<>(new Vector<>(songmood.getResultList())));
		cbMood.setBounds(109, 118, 144, 20);
		panel_1.add(cbMood);

		JButton btnClear = new JButton("ล้างค่า");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetField();
			}
		});
		btnClear.setBounds(210, 384, 89, 23);
		getContentPane().add(btnClear);

		JButton btnInsertNewSong = new JButton("เพิ่ม");
		btnInsertNewSong.setBounds(111, 384, 89, 23);
		getContentPane().add(btnInsertNewSong);

		JButton btnCancel = new JButton("ยกเลิก");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closing();
			}
		});
		btnCancel.setBounds(309, 384, 89, 23);
		getContentPane().add(btnCancel);
		btnInsertNewSong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(radDetail.isSelected()){
					String songName, mood, lyricist, melody, genre, tempo;
					songName = tfSongName.getText();
					mood = cbMood.getSelectedItem().toString();
					lyricist = tfLyrics.getText();
					melody = tfMelody.getText();
					genre = cbGenre.getSelectedItem().toString();
					tempo = cbTempo.getSelectedItem().toString();
					int year = Integer.parseInt(spYear.getValue().toString());
					
					if(newSong(songName, mood, lyricist, melody, genre, tempo, year, cbArtist.getSelectedItem().toString(), cbRecord.getSelectedItem().toString())) 
						JOptionPane.showMessageDialog(InsertWindows.this, "เพิ่มเพลงสำเร็จ");
					else 
						JOptionPane.showMessageDialog(InsertWindows.this, "ไม่สามารถเพิ่มเพลงได้");
					resetField();
				} else {
					/***
					 * Add new song from file
					 */
					Vector<String> error = new Vector<>();
					File inputFile = new File(tfFileLocation.getText());
					int lineNumber = 0;
					String line;
					try {
						@SuppressWarnings("resource")
						BufferedReader reader = new BufferedReader(new FileReader(inputFile));
						while((line=reader.readLine()) != null){
							++lineNumber;
							StringTokenizer tokenizer = new StringTokenizer(line, ",");
							if(tokenizer.countTokens() != 9){
								throw new Exception("จำนวนข้อมูลผิดพลาด");
							}
							String songName, artist, record, genre, songMood, releaseYear, tempo, lyricist, melodywriter;
							songName = tokenizer.nextToken();
							artist = tokenizer.nextToken();
							record = tokenizer.nextToken();
							genre = tokenizer.nextToken();
							songMood = tokenizer.nextToken();
							releaseYear = tokenizer.nextToken();
							tempo = tokenizer.nextToken();
							lyricist = tokenizer.nextToken();
							melodywriter = tokenizer.nextToken();
							
							if(!newSong(songName, songMood, lyricist, melodywriter, genre, tempo, Integer.parseInt(releaseYear), artist, record)){
								throw new Exception("ไม่สามารถเพิ่มเพลงได้ อาจเพราะมีเพลงนี้อยู่แล้ว โปรดตรวจสอบอีกครั้งก่อนทำการเพิ่มเพลงนี้");
							}
						}
						reader.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (Exception e1) {
						error.add("In line " + lineNumber + ":" + e1.getMessage());
					}
					String errorMSG = "";
					for(String s: error){
						errorMSG += s + "\n";
					}
					JOptionPane.showMessageDialog(InsertWindows.this, errorMSG, "ผิดพลาด", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	protected boolean newSong(String songName, String mood, String lyricist, 
			String melody, String genre, String tempo, int year, String artist, String record) {
		try {
			if(SongService.hasSong(songName, artist)) {
				return false;
			}
			else {
				return SongService.newSong(songName, artist, record, genre, mood, year, tempo, lyricist, melody);				
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void closing() {
//		callback.setJTableData(new DefaultTableModel(TableDataService.loadTableData(), new Vector<String>(TableDataService.getColumnNames())));
		callback.resetTable(3);
		dispose();
		instance = null;
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		closing();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		switch (e.getActionCommand()) {
		case "เพิ่มจากไฟล์ CSV":
			disableDetail();
			break;
		case "เพิ่มจากข้อมูล":
			disableFromFile();
			break;
		}
	}

	private void disableFromFile() {
		for (Component comp : panel.getComponents()) {
			comp.setEnabled(false);
		}
		for (Component comp : panel_1.getComponents()) {
			comp.setEnabled(true);
		}
	}

	private void disableDetail() {
		for (Component comp : panel_1.getComponents()) {
			comp.setEnabled(false);
		}
		for (Component comp : panel.getComponents()) {
			comp.setEnabled(true);
		}
	}

	public void invalidateArtist() {
		TypedQuery<Artist> q = GeneralQueryService.find("Artist.findAll", Artist.class);
		cbArtist.setModel(new DefaultComboBoxModel<>(new Vector<>(q.getResultList())));
		invalidate();
	}

	public void invalidateRecord() {
		TypedQuery<Record> q = GeneralQueryService.find("Record.findAll", Record.class);
		cbRecord.setModel(new DefaultComboBoxModel<>(new Vector<>(q.getResultList())));
		invalidate();
	}

	public void resetField() {
		tfSongName.setText("");
		cbArtist.setSelectedIndex(0);
		cbRecord.setSelectedIndex(0);
		cbGenre.setSelectedIndex(0);
		cbTempo.setSelectedIndex(0);
		cbMood.setSelectedIndex(0);
		spYear.setValue(Calendar.getInstance().get(Calendar.YEAR)+543);
		tfMelody.setText("");
		tfLyrics.setText("");
		invalidate();
	}
}
