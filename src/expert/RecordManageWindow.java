package expert;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import entity.Record;
import main.MainThread;
import service.RecordService;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class RecordManageWindow extends JFrame implements WindowListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static RecordManageWindow instance;
	private static JFrame callback;
	private JComboBox<String> comboRecord;

	/**
	 * Create the panel.
	 */
	private RecordManageWindow() {
		getContentPane().setLayout(null);
		initialize();
		setVisible(true);
	}

	private void initialize() {
		setSize(330, 130);
		setTitle("\u0e08\u0e31\u0e14\u0e01\u0e32\u0e23\u0e2a\u0e31\u0e07\u0e01\u0e31\u0e14");
		JLabel lblNewLabel = new JLabel("\u0e2a\u0e31\u0e07\u0e01\u0e31\u0e14");
		lblNewLabel.setBounds(10, 11, 46, 14);
		getContentPane().add(lblNewLabel);

		comboRecord = new JComboBox<>();
		comboRecord.setBounds(66, 8, 231, 20);
		loadModel();
		getContentPane().add(comboRecord);

		JButton btnAdd = new JButton("\u0e40\u0e1e\u0e34\u0e48\u0e21");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddRecord.instantiate(RecordManageWindow.this);
			}
		});
		btnAdd.setBounds(10, 55, 89, 23);
		getContentPane().add(btnAdd);

		JButton btnMod = new JButton("\u0e41\u0e01\u0e49\u0e44\u0e02");
		btnMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Record selected = RecordService.findRecordByName(comboRecord.getSelectedItem().toString());
				EditRecordWindow.instantiate(RecordManageWindow.this, selected, comboRecord.getSelectedIndex());
			}
		});
		btnMod.setBounds(109, 55, 89, 23);
		getContentPane().add(btnMod);

		JButton btnDel = new JButton("ลบ");
		btnDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Record selected = RecordService.findArtistByName(comboRecord.getSelectedItem().toString());
				int optionSelected = JOptionPane.showConfirmDialog(btnDel, "\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23\u0e25\u0e1a  " + selected.getRName() + " \u0e2b\u0e23\u0e37\u0e2d\u0e44\u0e21\u0e48", "\u0e25\u0e1a\u0e2a\u0e31\u0e07\u0e01\u0e31\u0e14", JOptionPane.OK_CANCEL_OPTION);
				if(optionSelected == JOptionPane.OK_OPTION) {
					try {
						MainThread.em.getTransaction().begin();
						MainThread.em.remove(selected);
						MainThread.em.getTransaction().commit();
						JOptionPane.showMessageDialog(btnDel, "\u0e25\u0e1a\u0e28\u0e34\u0e25\u0e1b\u0e34\u0e19\u0e2a\u0e33\u0e40\u0e23\u0e47\u0e08");
						loadModel();
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(btnDel, "\u0e44\u0e21\u0e48\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16\u0e25\u0e1a\u0e28\u0e34\u0e25\u0e1b\u0e34\u0e19\u0e44\u0e14\u0e49");
					}
				}
			}
		});
		btnDel.setBounds(208, 55, 89, 23);
		getContentPane().add(btnDel);
		addWindowListener(this);
	}

	public static void instantiate(JFrame c) {
		callback = c;
		if (instance == null)
			instance = new RecordManageWindow();
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void setArtistComboModel(ComboBoxModel<String> model) {
		comboRecord.setModel(model);
		invalidate();
	}

	@Override
	public void windowClosed(WindowEvent e) {
		closing();
	}

	private void closing() {
		callback = null;
		instance = null;
		dispose();

	}

	@Override
	public void windowClosing(WindowEvent e) {
		if(callback instanceof InsertWindows){
			((InsertWindows) callback).invalidateRecord();
		} else if(callback instanceof EditWindow){
			((EditWindow) callback).invalidateRecord();
		}
		closing();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void loadModel(int selectedRow) {
		loadModel();
		comboRecord.setSelectedIndex(selectedRow);
		invalidate();
	}

	public void loadModel() {
		List<String> result = RecordService.getAllRecordName();
		Vector<String> v = new Vector<String>(result);
		ComboBoxModel<String> model = new DefaultComboBoxModel<>(v);
		setArtistComboModel(model);
	}
}
