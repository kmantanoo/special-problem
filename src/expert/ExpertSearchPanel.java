package expert;

import javax.swing.JPanel;

import entity.Song;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import mainsearch.CardsPanel;
import mainsearch.SearchAndClearButtonListener;
import service.SongService;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class ExpertSearchPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CardsPanel cards;
	private JButton btnClear, btnEditData, btnDeleteSong, btnChangeMode, btnSearch, btnInsert;
	private JButton btnInstanceStat;
	private ExpertMainSearchPanel view;
	
	/**
	 * Create the panel.
	 * @param view 
	 */
	public ExpertSearchPanel(ExpertMainSearchPanel view) {
		this.view = view;
		btnInsert = new JButton("เพิ่มข้อมูล");
		btnInsert.setLocation(506, 9);
		btnInsert.setSize(89, 23);
		btnInsert.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				InsertWindows.instantiate(view);
			}
		});
		add(btnInsert);
		
		btnClear = new JButton("ล้างการค้นหา");
		btnClear.addActionListener(new SearchAndClearButtonListener(cards, view));
		btnClear.setLocation(605, 9);
		btnClear.setSize(93, 23);
		add(btnClear);
		
		btnEditData = new JButton("แก้ไขเพลง");
		btnEditData.addActionListener(new EditDeleteListener());
		btnEditData.setLocation(807, 9);
		btnEditData.setSize(89, 23);
		add(btnEditData);
		
		btnDeleteSong = new JButton("ลบเพลง");
		btnDeleteSong.addActionListener(new EditDeleteListener());
		btnDeleteSong.setLocation(708, 9);
		btnDeleteSong.setSize(89, 23);
		add(btnDeleteSong);
		
		btnEditData.setEnabled(false);
		btnDeleteSong.setEnabled(false);
		
		btnInstanceStat = new JButton("ข้อมูลเชิงสถิติ");
		btnInstanceStat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InstanceStat.instantiate();
			}
		});
		btnInstanceStat.setLocation(906, 9);
		btnInstanceStat.setSize(97, 23);
		add(btnInstanceStat);
		
		
		
		btnChangeMode = new JButton("เปลี่ยนเงื่อนไขการค้นหา");
		btnChangeMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cards.switchMode();
				invalidate();
			}
		});
		btnChangeMode.setBounds(1013, 9, 158, 23);
		setLayout(null);
		add(btnChangeMode);
		cards = new CardsPanel();
		cards.setBounds(0, 0, 397, 41);
		add(cards);
		setPreferredSize(new Dimension(1183, 40));
		setBounds(0, 0, 1114, 40);
		
		btnSearch = new JButton("ค้นหา");
		btnSearch.addActionListener(new SearchAndClearButtonListener(cards, view));
		btnSearch.setBounds(407, 9, 89, 23);
		add(btnSearch);
	}
	
	public void enableEditAndDeleteSongButton(boolean tf){
		btnEditData.setEnabled(tf);
		btnDeleteSong.setEnabled(tf);
	}
	
	class EditDeleteListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("ลบเพลง")){
				int selection = JOptionPane.showConfirmDialog(ExpertSearchPanel.this, "ต้องการลบเพลง \"" + view.getSelectedRow().get(0) + "\" ใช่หรือไม่", "ลบเพลง", JOptionPane.OK_CANCEL_OPTION);
				if(selection == JOptionPane.OK_OPTION){
					delete();
				}
			} else {
				Vector<String> v = view.getSelectedRow();
				Song found = SongService.findSong(v.get(0), v.get(1), Integer.parseInt(v.get(6))) ;
				EditWindow.instantiate(view, found);
			}
		}
		
		private void delete(){
			Vector<String> songData = view.getSelectedRow();
			String sName, aName, year;
			sName = songData.get(0);
			aName = songData.get(1);
			year = songData.get(6);
			if(SongService.deleteSong(sName, aName, Integer.parseInt(year))){
				JOptionPane.showMessageDialog(ExpertSearchPanel.this, "ลบเพลงสำเร็จ", "ผลลัพธ์", JOptionPane.INFORMATION_MESSAGE);
				view.resetTable(3);
			} else {
				JOptionPane.showMessageDialog(ExpertSearchPanel.this, "ลบเพลงไม่สำเร็จ", "ผลลัพธ์", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
}
