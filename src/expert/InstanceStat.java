package expert;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import service.ArtistService;
import service.RecordService;
import service.SongService;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InstanceStat extends JFrame implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static InstanceStat instance;
	private JTextField songTF;
	private JTextField artistTF;
	private JTextField moodTF;
	private JTextField recordTF;
	
	private InstanceStat(){
		initialize();
		setVisible(true);
	}
	
	private void initialize() {
		setBounds(600, 200, 250, 230);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("จำนวนเพลง");
		lblNewLabel.setBounds(10, 11, 61, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel label = new JLabel("จำนวนศิลปิน");
		label.setBounds(10, 46, 61, 14);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("จำนวนรูปแบบอารมณ์เพลง");
		label_1.setBounds(10, 83, 134, 14);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("จำนวนสังกัด");
		label_2.setBounds(10, 120, 61, 14);
		getContentPane().add(label_2);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				instance = null;
			}
		});
		btnOk.setBounds(81, 148, 89, 23);
		getContentPane().add(btnOk);
		
		songTF = new JTextField();
		songTF.setText(Integer.toString(SongService.getSongCount()));
		songTF.setHorizontalAlignment(SwingConstants.RIGHT);
		songTF.setEditable(false);
		songTF.setBounds(81, 8, 86, 20);
		getContentPane().add(songTF);
		songTF.setColumns(10);
		
		artistTF = new JTextField();
		artistTF.setHorizontalAlignment(SwingConstants.RIGHT);
		artistTF.setText(Integer.toString(ArtistService.getArtistCount()));
		artistTF.setEditable(false);
		artistTF.setBounds(81, 43, 86, 20);
		getContentPane().add(artistTF);
		artistTF.setColumns(10);
		
		moodTF = new JTextField();
		moodTF.setHorizontalAlignment(SwingConstants.RIGHT);
		moodTF.setText(Integer.toString(SongService.getMoodCount()));
		moodTF.setEditable(false);
		moodTF.setBounds(140, 80, 86, 20);
		getContentPane().add(moodTF);
		moodTF.setColumns(10);
		
		recordTF = new JTextField();
		recordTF.setHorizontalAlignment(SwingConstants.RIGHT);
		recordTF.setText(Integer.toString(RecordService.getRecordCount()));
		recordTF.setEditable(false);
		recordTF.setBounds(81, 117, 86, 20);
		getContentPane().add(recordTF);
		recordTF.setColumns(10);
		
	}
	
	public static void instantiate(){
		
		if(instance == null) instance = new  InstanceStat();
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		dispose();
		instance = null;
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
