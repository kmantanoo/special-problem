package expert;

import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JTable;

import mainsearch.FilterPanel;

public class ExpertMainWindow extends JFrame implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ExpertMainWindow instance;
	private FilterPanel ftPanel;
	private ExpertMainSearchPanel expmsPanel;
//	private MainSearchPanel expmsPanel;
	/**
	 * Create the application.
	 */
	private ExpertMainWindow() {
		initialize();
		addWindowListener(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setLocation(550, 100);
		setSize(1200, 600);
		setTitle("Expert User Mode");
		getContentPane().add(expmsPanel = new ExpertMainSearchPanel(this));
		getContentPane().add(ftPanel = new FilterPanel(this), BorderLayout.SOUTH);
		
		ftPanel.updateFilter(expmsPanel.getTable());
		
		setVisible(true);
	}
	
	public static void instantiate(){
		if(instance == null) {
			instance = new ExpertMainWindow();
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		dispose();
		instance=null;
		AuthenticationWindow.clearInstance();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	public void updateFilter(JTable table) {
		ftPanel.updateFilter(table);
	}

	public void applyFilter(HashMap<String, String> filter) {
		expmsPanel.applyFilter(filter);		
	}

	public void resetTable(int i) {
		expmsPanel.resetTable(i);
	}

}
