package expert;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import entity.Artist;
import entity.Genre;
import entity.Record;
import entity.Song;
import entity.Songmood;
import entity.Tempo;
import service.GeneralQueryService;
import service.SongService;
import javax.swing.JTextField;
import javax.persistence.TypedQuery;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JSpinner;

public class EditWindow extends JFrame implements WindowListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static EditWindow instance;
	private JComboBox<Object> cbArtist, cbRecord, cbTempo, cbGenre, cbMood;
	private JPanel panel_1;
	private JTextField tfSongName;
	private JTextField tfLyrics;
	private JTextField tfMelody;
	private JSpinner spYear;
	private static ExpertMainSearchPanel callback;
	private Song s;

	public static void instantiate(ExpertMainSearchPanel expertMainSearchPanel, Song selectedSong) {
		callback = expertMainSearchPanel;
		if (instance == null)
			instance = new EditWindow(selectedSong);
	}

	/**
	 * Create the application.
	 * @param selectedSong 
	 */
	public EditWindow(Song selectedSong) {
		s = selectedSong;
		initialize();
		addWindowListener(this);
		resetField();
		setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() {

		setTitle("แก้ไขข้อมูล");
		setBounds(100, 100, 522, 357);
		getContentPane().setLayout(null);

		panel_1 = new JPanel();
		panel_1.setName("detail");
		TitledBorder titleTwo = new TitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		titleTwo.setTitle("Detail");
		panel_1.setBorder(titleTwo);
		panel_1.setBounds(10, 11, 478, 261);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("เพลง");
		lblNewLabel_1.setBounds(10, 14, 46, 14);
		panel_1.add(lblNewLabel_1);

		tfSongName = new JTextField();
		tfSongName.setBounds(109, 11, 173, 20);
		panel_1.add(tfSongName);
		tfSongName.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("ศิลปิน");
		lblNewLabel_2.setBounds(10, 39, 46, 14);
		panel_1.add(lblNewLabel_2);

		cbArtist = new JComboBox<>();
		cbArtist.setBounds(109, 36, 144, 20);
		invalidateArtist();
		
		panel_1.add(cbArtist);

		JButton btnNewArtist = new JButton("จัดการศิลปิน");
		btnNewArtist.setBounds(366, 32, 102, 23);
		panel_1.add(btnNewArtist);
		btnNewArtist.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArtistManageWindow.instantiate(EditWindow.this);
			}
		});

		JLabel lblNewLabel_3 = new JLabel("สังกัด");
		lblNewLabel_3.setBounds(10, 64, 46, 14);
		panel_1.add(lblNewLabel_3);

		cbRecord = new JComboBox<>();
		cbRecord.setBounds(109, 61, 144, 20);
		invalidateRecord();
		
		
		panel_1.add(cbRecord);

		JLabel lblNewLabel_4 = new JLabel("ปีที่ปล่อย");
		lblNewLabel_4.setBounds(10, 149, 46, 14);
		panel_1.add(lblNewLabel_4);

		spYear = new JSpinner();
		spYear.setBounds(109, 146, 63, 20);
		spYear.setEditor(new JSpinner.NumberEditor(spYear, "#"));
		spYear.setValue(s.getReleaseyear());
		panel_1.add(spYear);

		JButton btnNewRecord = new JButton("จัดการสังกัด");
		btnNewRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RecordManageWindow.instantiate(EditWindow.this);
			}
		});
		btnNewRecord.setBounds(366, 57, 102, 23);
		panel_1.add(btnNewRecord);

		JLabel lblNewLabel_5 = new JLabel(
				"ผู้ประพันธ์เนื้อร้อง");
		lblNewLabel_5.setBounds(10, 208, 79, 14);
		panel_1.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel(
				"ผู้ประพันธ์ทำนอง");
		lblNewLabel_6.setBounds(10, 233, 79, 14);
		panel_1.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("แนวเพลง");
		lblNewLabel_7.setBounds(10, 90, 46, 14);
		panel_1.add(lblNewLabel_7);

		TypedQuery<Genre> genre = GeneralQueryService.find("Genre.findAll", Genre.class);
		cbGenre = new JComboBox<>();
		cbGenre.setBounds(109, 87, 79, 20);
		cbGenre.setModel(new DefaultComboBoxModel<>(new Vector<>(genre.getResultList())));
		
		panel_1.add(cbGenre);

		tfLyrics = new JTextField();
		tfLyrics.setBounds(109, 205, 144, 20);
		panel_1.add(tfLyrics);
		tfLyrics.setColumns(10);

		tfMelody = new JTextField();
		tfMelody.setBounds(109, 230, 144, 20);
		panel_1.add(tfMelody);
		tfMelody.setColumns(10);

		JLabel lblNewLabel_8 = new JLabel("จังหวะเพลง");
		lblNewLabel_8.setBounds(10, 180, 63, 14);
		panel_1.add(lblNewLabel_8);

		TypedQuery<Tempo> tempo = GeneralQueryService.find("Tempo.findAll", Tempo.class);
		cbTempo = new JComboBox<>();
		cbTempo.setBounds(109, 177, 79, 20);
		cbTempo.setModel(new DefaultComboBoxModel<>(new Vector<>(tempo.getResultList())));
		
		panel_1.add(cbTempo);

		JLabel lblNewLabel_9 = new JLabel("อารมณ์เพลง");
		lblNewLabel_9.setBounds(10, 121, 63, 14);
		panel_1.add(lblNewLabel_9);
		
		TypedQuery<Songmood> songmood = GeneralQueryService.find("Songmood.findAll", Songmood.class);
		cbMood = new JComboBox();
		cbMood.setModel(new DefaultComboBoxModel<>(new Vector<>(songmood.getResultList())));
		cbMood.setBounds(109, 118, 144, 20);
		panel_1.add(cbMood);

		JButton btnClear = new JButton("ล้างค่า");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetField();
			}
		});
		btnClear.setBounds(208, 283, 89, 23);
		getContentPane().add(btnClear);

		JButton btnSave = new JButton("บันทึก");
		btnSave.setBounds(109, 283, 89, 23);
		getContentPane().add(btnSave);

		JButton btnCancel = new JButton("ยกเลิก");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closing();
			}
		});
		btnCancel.setBounds(307, 283, 89, 23);
		getContentPane().add(btnCancel);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selection = JOptionPane.showConfirmDialog(EditWindow.this, "ต้องการบันทึกใช่หรือไม่", "บันทึก", JOptionPane.OK_CANCEL_OPTION);
				if(selection == JOptionPane.OK_OPTION){
					/***
					 * perform update song info
					 */
					try {
						Vector<String> songInfo = new Vector<>();
						songInfo.add(tfSongName.getText());
						songInfo.add(cbArtist.getSelectedItem().toString());
						songInfo.add(cbRecord.getSelectedItem().toString());
						songInfo.add(cbGenre.getSelectedItem().toString());
						songInfo.add(cbMood.getSelectedItem().toString());
						songInfo.add(spYear.getValue().toString());
						songInfo.add(cbTempo.getSelectedItem().toString());
						songInfo.add(tfLyrics.getText());
						songInfo.add(tfMelody.getText());
						SongService.updateSong(s, songInfo);
						JOptionPane.showMessageDialog(EditWindow.this, "แก้ไขเพลงเรียบร้อย", "สำเร็จ", JOptionPane.INFORMATION_MESSAGE);
						closing();						
					} catch (Exception e2) {
						JOptionPane.showMessageDialog(EditWindow.this, "ไม่สามารถแก้ไขเพลงได้", "ข้อผิดพลาด", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}

	protected boolean newSong(String songName, String mood, String lyricist, 
			String melody, String genre, String tempo, int year, String artist, String record) {
		try {
			if(SongService.hasSong(songName, artist)) {
				return false;
			}
			else {
				return SongService.newSong(songName, artist, record, genre, mood, year, tempo, lyricist, melody);				
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void closing() {
		callback.resetTable(3);
		dispose();
		instance = null;
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		closing();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public void invalidateArtist() {
		TypedQuery<Artist> q = GeneralQueryService.find("Artist.findAll", Artist.class);
		cbArtist.setModel(new DefaultComboBoxModel<>(new Vector<>(q.getResultList())));
		invalidate();
	}

	public void invalidateRecord() {
		TypedQuery<Record> q = GeneralQueryService.find("Record.findAll", Record.class);
		cbRecord.setModel(new DefaultComboBoxModel<>(new Vector<>(q.getResultList())));
		invalidate();
	}

	public void resetField() {
		tfSongName.setText(s.getSongname());
		cbArtist.setSelectedItem(s.getCreator().getArtist());
		cbRecord.setSelectedItem(s.getCreator().getRecord());
		cbGenre.setSelectedItem(s.getGenre());
		cbTempo.setSelectedItem(s.getTempo());
		cbMood.setSelectedItem(s.getSongmood());
		spYear.setValue(s.getReleaseyear());
		tfMelody.setText(s.getCreator().getMelodywriter());
		tfLyrics.setText(s.getCreator().getLyricist());
		invalidate();
	}
}
