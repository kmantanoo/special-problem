package main;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import service.CacheThread;
import service.FileService;
import service.SongStatusService;

public class StartAppWindow implements WindowListener{

	private JFrame frame;

	/**
	 * Create the application.
	 */
	public StartAppWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			//			UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileService.createAllCachedTemp();

		frame = new JFrame();
		frame.setBounds(100, 100, 460, 385);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Semantic Song Search");



		frame.getContentPane().add(new MainPanel());
		frame.addWindowListener(this);
		frame.setVisible(true);
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
//		System.out.println("Closed");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		FileService.clearCacheTemp();
		if(SongStatusService.getChangeStatus()){
			CacheThread.startCache();
		}
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
