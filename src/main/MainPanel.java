package main;
import javax.swing.JPanel;

import expert.AuthenticationWindow;
import mainsearch.MainSearchMainWindow;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class MainPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Create the panel.
	 */
	public MainPanel() {
		
		setLayout(null);
		
		// Regular User Button
		JButton userModeButton = new JButton("ผู้ใช้ทั่วไป");
		userModeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainSearchMainWindow.instantiate();
			}
		});
		userModeButton.setBounds(284, 105, 115, 45);
		add(userModeButton);
		
		JButton expertModeButton = new JButton("ผู้ดูแลระบบ");
		expertModeButton.setBounds(284, 192, 115, 45);
		expertModeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				AuthenticationWindow.instantiate();
			}
		});
		add(expertModeButton);
		
		JTextPane textPane = new JTextPane();
		textPane.setText("ผู้จัดทำ\nนายกฤษดา\tแม่นธนู\t5510450037\nนายชานน\tรุ่งรัศมีเจริญ\t5510450134");
		textPane.setEditable(false);
		textPane.setEnabled(false);
		textPane.setBounds(228, 289, 212, 45);
		add(textPane);
		
		JLabel imageLabel = new JLabel("");
		imageLabel.setIcon(new ImageIcon(MainPanel.class.getResource("/main.png")));
		imageLabel.setBounds(10, 11, 440, 431);
		
		add(imageLabel);

	}
}
