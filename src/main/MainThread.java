package main;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import service.CacheThread;
import service.FileService;
import service.SongStatusService;

public class MainThread {
	public static EntityManager em = Persistence.createEntityManagerFactory("SeniorProject_JPA").createEntityManager();
	public static void main(String[] args) {
		new StartAppWindow();
	}
}
