package main;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

public class SongDetailWindow extends JFrame implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static SongDetailWindow instance;

	/**
	 * Create the application.
	 */
	private SongDetailWindow() {
		initialize();
		setVisible(true);
	}
	
	public static void instantiate() {
		if(instance == null) instance = new SongDetailWindow();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25\u0e02\u0e2d\u0e07\u0e40\u0e1e\u0e25\u0e07");
		setBounds(200, 200, 251, 286);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u0E40\u0E1E\u0E25\u0E07");
		lblNewLabel.setBounds(10, 11, 46, 14);
		getContentPane().add(lblNewLabel);
		
		JLabel labelSongName = new JLabel("");
		labelSongName.setBounds(87, 11, 129, 14);
		getContentPane().add(labelSongName);
		
		JLabel lblNewLabel_2 = new JLabel("\u0E28\u0E34\u0E25\u0E1B\u0E34\u0E19");
		lblNewLabel_2.setBounds(10, 36, 46, 14);
		getContentPane().add(lblNewLabel_2);
		
		JLabel labelArtist = new JLabel("");
		labelArtist.setBounds(87, 36, 129, 14);
		getContentPane().add(labelArtist);
		
		JLabel label_1 = new JLabel("\u0E41\u0E19\u0E27\u0E40\u0E1E\u0E25\u0E07");
		label_1.setBounds(10, 61, 46, 14);
		getContentPane().add(label_1);
		
		JLabel labelGenre = new JLabel("");
		labelGenre.setBounds(87, 61, 129, 14);
		getContentPane().add(labelGenre);
		
		JLabel label_3 = new JLabel("\u0E2D\u0E32\u0E23\u0E21\u0E13\u0E4C\u0E40\u0E1E\u0E25\u0E07");
		label_3.setBounds(10, 86, 67, 14);
		getContentPane().add(label_3);
		
		JLabel labelSongMood = new JLabel("");
		labelSongMood.setBounds(87, 86, 129, 14);
		getContentPane().add(labelSongMood);
		
		JLabel label_5 = new JLabel("\u0E2A\u0E31\u0E07\u0E01\u0E31\u0E14");
		label_5.setBounds(10, 111, 46, 14);
		getContentPane().add(label_5);
		
		JLabel labelRecord = new JLabel("");
		labelRecord.setBounds(87, 111, 129, 14);
		getContentPane().add(labelRecord);
		
		JLabel label_7 = new JLabel("\u0E1C\u0E39\u0E49\u0E41\u0E15\u0E48\u0E07\u0E40\u0E19\u0E37\u0E49\u0E2D\u0E23\u0E49\u0E2D\u0E07");
		label_7.setBounds(10, 136, 67, 14);
		getContentPane().add(label_7);
		
		JLabel labelLyricDirector = new JLabel("");
		labelLyricDirector.setBounds(87, 136, 129, 14);
		getContentPane().add(labelLyricDirector);
		
		JLabel label_9 = new JLabel("\u0E1C\u0E39\u0E49\u0E41\u0E15\u0E48\u0E07\u0E17\u0E33\u0E19\u0E2D\u0E07");
		label_9.setBounds(10, 161, 67, 14);
		getContentPane().add(label_9);
		
		JLabel label_10 = new JLabel("\u0E1B\u0E35\u0E17\u0E35\u0E48\u0E1B\u0E25\u0E48\u0E2D\u0E22");
		label_10.setBounds(10, 186, 46, 14);
		getContentPane().add(label_10);
		
		JLabel labelMelodyDirector = new JLabel("");
		labelMelodyDirector.setBounds(87, 161, 129, 14);
		getContentPane().add(labelMelodyDirector);
		
		JLabel labelYearReleased = new JLabel("");
		labelYearReleased.setBounds(87, 186, 129, 14);
		getContentPane().add(labelYearReleased);
		
		JButton btnOk = new JButton("\u0E15\u0E01\u0E25\u0E07");
		btnOk.setBounds(74, 212, 89, 23);
		getContentPane().add(btnOk);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		dispose();
		instance = null;
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
