package mainsearch;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.BoxLayout;

public class MainSearchMainWindow extends JFrame implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static MainSearchMainWindow instance;
	private MainSearchPanel msPanel;
	private FilterPanel ftPanel;
	/**
	 * Create the application.
	 */
	private MainSearchMainWindow() {
		initialize();
		addWindowListener(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setLocation(550, 100);
		setSize(1100, 600);
		setTitle("สืบค้นจากเหตุการณ์");
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		getContentPane().add(msPanel = new MainSearchPanel(this));
		getContentPane().add(ftPanel = new FilterPanel(this));
		
		ftPanel.updateFilter(msPanel.getTable());
		setVisible(true);
	}
	
	public static void instantiate(){
		if(instance == null) {
			instance = new MainSearchMainWindow();
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		dispose();
		instance=null;
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	
	public void updateFilter(JTable table) {
		ftPanel.updateFilter(table);
	}
	
	public void applyFilter(HashMap<String, String> filter){
		msPanel.applyFilter(filter);
	}

	public void resetTable(int callerStatus) {
		msPanel.resetTable(callerStatus);
	}

}
