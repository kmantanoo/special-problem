package mainsearch;

import javax.swing.JPanel;

import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SearchPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CardsPanel cards;
	private JButton btnSearch;
	/**
	 * Create the panel.
	 */
	public SearchPanel(MainSearchPanel view) {
		
		JButton btnChangeMode = new JButton("เปลี่ยนเงื่อนไขการค้นหา");
		btnChangeMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cards.switchMode();
				invalidate();
			}
		});
		btnChangeMode.setBounds(884, 9, 143, 23);
		setLayout(null);
		add(btnChangeMode);
		cards = new CardsPanel();
		cards.setBounds(0, 0, 438, 41);
		add(cards);
		setPreferredSize(new Dimension(1038, 40));
		setBounds(0, 0, 898, 40);
		
		btnSearch = new JButton("\u0E04\u0E49\u0E19\u0E2B\u0E32");
		btnSearch.addActionListener(new SearchAndClearButtonListener(cards, view));
		btnSearch.setBounds(668, 9, 89, 23);
		add(btnSearch);
		
		JButton btnClear = new JButton("ล้างการค้นหา");
		btnClear.addActionListener(new SearchAndClearButtonListener(cards, view));
		btnClear.setBounds(767, 9, 107, 23);
		add(btnClear);
	}
}
