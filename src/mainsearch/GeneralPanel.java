package mainsearch;

import javax.swing.JPanel;

import com.sun.prism.impl.Disposer.Record;

import entity.Artist;
import entity.Genre;
import entity.Songmood;
import entity.Tempo;
import service.GeneralQueryService;

import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class GeneralPanel extends JPanel {
	private static String[] mode = new String[]{"ศิลปิน", "อารมณ์เพลง", "สังกัด", "แนวเพลง", "จังหวะเพลง"};

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel labelCategory;
	private JComboBox<String> comboMode, comboValue;
	@SuppressWarnings("rawtypes")
	private HashMap<String, Vector> valueStorage;

	/**
	 * Create the panel.
	 */
	@SuppressWarnings("unchecked")
	public GeneralPanel() {
		
		JLabel mainLabel = new JLabel("\u0E2B\u0E21\u0E27\u0E14");
		mainLabel.setBounds(10, 8, 26, 14);
		
		valueStorage = new HashMap<>();
		loadDataToStorage();

		labelCategory = new JLabel("\u0E28\u0E34\u0E25\u0E1B\u0E34\u0E19");
		labelCategory.setBounds(175, 8, 28, 14);
		
		comboValue = new JComboBox<String>(valueStorage.get(mode[0]));
		comboValue.setBounds(207, 5, 149, 20);
		
		comboMode = new JComboBox<String>(new Vector<String>(Arrays.asList(mode)));
		comboMode.setBounds(41, 5, 129, 20);
		

		comboMode.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				@SuppressWarnings("rawtypes")
				JComboBox c = (JComboBox) e.getSource();
				comboValue.setModel(new DefaultComboBoxModel<>(valueStorage.get(c.getSelectedItem())));
			}
		});
		
		
		setLayout(null);
		add(mainLabel);
		add(comboMode);
		add(labelCategory);
		add(comboValue);

	}
	
	private void loadDataToStorage(){
		Vector<Artist> artists = new Vector<>(GeneralQueryService.find("Artist.findAll", Artist.class).getResultList());
		Vector<Record> records = new Vector<>(GeneralQueryService.find("Record.findAll", Record.class).getResultList());
		Vector<Songmood> songmoods = new Vector<>(GeneralQueryService.find("Songmood.findAll", Songmood.class).getResultList());
		Vector<Tempo> tempos = new Vector<>(GeneralQueryService.find("Tempo.findAll", Tempo.class).getResultList());
		Vector<Genre> genres = new Vector<>(GeneralQueryService.find("Genre.findAll", Genre.class).getResultList());
		
		valueStorage.put("ศิลปิน", artists);
		valueStorage.put("สังกัด", records);
		valueStorage.put("อารมณ์เพลง", songmoods);
		valueStorage.put("แนวเพลง", genres);
		valueStorage.put("จังหวะเพลง", tempos);
	}

	public Vector<String> getSelectedItem() {
		Vector<String> result = new Vector<>();
		result.add(comboMode.getSelectedItem().toString());
		result.add(comboValue.getSelectedItem().toString());
		return result;
	}
}
