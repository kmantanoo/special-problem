package mainsearch;

import javax.swing.JPanel;

import service.GeneralQueryService;

import javax.swing.JLabel;

import java.awt.Event;
import java.util.Arrays;
import java.util.Vector;

import javax.persistence.TypedQuery;
import javax.swing.JComboBox;

public class EventPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JComboBox<Event> comboMood;

	/**
	 * Create the panel.
	 */
	public EventPanel() {
		
		JLabel mainLabel = new JLabel("เหตุการณ์");
		mainLabel.setBounds(10, 8, 78, 14);
		
//		List<Event> events = GeneralQueryService.executeQuery("event_label", "%", "Event", Event.class);
		TypedQuery<Event> events = GeneralQueryService.find("Event.findAll", Event.class);
		Vector<Event> ev = new Vector<>(events.getResultList());
		comboMood = new JComboBox<>(ev);
		comboMood.setBounds(78, 5, 121, 20);
		setLayout(null);
		add(mainLabel);
		add(comboMood);

	}
	
	public Vector<String> getSelectedItem(){
		return new Vector<String>(Arrays.asList(comboMood.getSelectedItem().toString()));
	}
}
