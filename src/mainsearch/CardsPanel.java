package mainsearch;

import javax.swing.JPanel;
import java.awt.CardLayout;
import java.util.Vector;

public class CardsPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String[] cardName = {"event", "general"};
	private static int INDEX = 0;
	private EventPanel eventCard;
	private GeneralPanel generalCard;

	/**
	 * Create the panel.
	 */
	public CardsPanel() {
		setLayout(new CardLayout(0, 5));
		
		eventCard = new EventPanel();
		generalCard = new GeneralPanel();
		
		add(eventCard, "event");
		add(generalCard, "general");

	}
	
	private void showCard(String name){
		CardLayout layout = (CardLayout) getLayout();
		layout.show(this, name);
	}
	
	public void switchMode() {
		INDEX = INDEX == 0 ? 1 : 0;
		showCard(cardName[INDEX]);
	}
	
	public Vector<String> getSelectedItem(){
		return INDEX==0 ? eventCard.getSelectedItem() : generalCard.getSelectedItem(); 
	}

}
