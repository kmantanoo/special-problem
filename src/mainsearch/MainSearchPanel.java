package mainsearch;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import service.FilterService;
import service.TableDataService;

import javax.swing.JTable;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class MainSearchPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTable table;
	private Vector<Vector<String>> initialTableData;
	private FilterService ftService;
	/**
	 * Create the panel.
	 */
	public MainSearchPanel(JFrame mainWindow) {
		
		setLayout(new BorderLayout(0, 0));
		
		SearchPanel searchPanel = new SearchPanel(this);
		add(searchPanel, BorderLayout.NORTH);
		createTable(initialTableData=TableDataService.loadTableData());
		ftService = new FilterService(initialTableData, table, this, mainWindow);
	}
	
	public void createTable(Vector<Vector<String>> data){
		Vector<String> cNames = new Vector<String>();
		cNames.addAll(TableDataService.getColumnNames());
		
		table = new JTable(data, cNames){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column){
				return false;
			}
		};
		
		
		table.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        if (me.getClickCount() == 2) {
		        	System.out.println(row + " double clicked!!");
		        	ArrayList<String> datas = new ArrayList<String>();
		        	for(int i = 1; i < table.getColumnCount(); i++){
		        		datas.add((String) table.getValueAt(row, i));
		        	}
		        	datas.add("-");
		        	System.out.println("Datas size " + datas.size());
		        	if(!SongDataWindow.haveInstance()) SongDataWindow.instantiate(datas);
		        	else SongDataWindow.updateData(datas);
		        }
		    }
		});
		
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setBackground(Color.WHITE);
		table.setBounds(24, 407, 615, -358);
		
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
	}
	
	public JTable getTable(){ return table;}

	public void applyFilter(HashMap<String, String> filter) {
		ftService.applyFilter(filter);
	}

	public void resetTable(int callerStatus) {
		ftService.resetTable(callerStatus);
	}

	public void updateTable(Vector<Vector<String>> data, boolean b, int i) {
		ftService.updateTable(data, b, i);
	}

	public void updateFilter() {
		ftService.updateFilter();
	}
	
}
