package mainsearch;

import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableModel;

import expert.ExpertMainWindow;

import javax.swing.JCheckBox;

public class FilterPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JComboBox<String> comboArtist, comboRecord, comboGenre, comboTempo;
	private JSpinner spinnerFromYear, spinnerToYear;
	private DefaultComboBoxModel<String> artistModel, recordModel, genreModel, tempoModel;
	private JCheckBox checkboxUseFilter;
	private JFrame mainWindow;
	private static FilterListener listener;

	/**
	 * Create the panel.
	 * @param mainSearchMainWindow 
	 */
	public FilterPanel(JFrame mainSearchMainWindow) {
		mainWindow = mainSearchMainWindow;
		
		initiate();
		invalidate();

		setEnabled(false);
		
		checkboxUseFilter = new JCheckBox("ใช้ฟิลเตอร์");
		checkboxUseFilter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(((JCheckBox)e.getSource()).isSelected()){
					performApplyFilter();
				} else {
					/***
					 * implement this when checkboxUseFilter is unchecked
					 */
					if(mainWindow instanceof MainSearchMainWindow){
						((MainSearchMainWindow) mainWindow).resetTable(2);
					} else {
						((ExpertMainWindow) mainWindow).resetTable(2);
					}
				}
			}
		});
		add(checkboxUseFilter);
	}
	
	private void performApplyFilter(){
		HashMap<String, String> filter = new HashMap<String, String>();
		filter.put("artist", comboArtist.getSelectedItem().toString());
		filter.put("record", comboRecord.getSelectedItem().toString());
		filter.put("genre", comboGenre.getSelectedItem().toString());
		filter.put("tempo", comboTempo.getSelectedItem().toString());
		filter.put("from", spinnerFromYear.getValue().toString());
		filter.put("to", spinnerToYear.getValue().toString());


		if(mainWindow instanceof MainSearchMainWindow){
			((MainSearchMainWindow) mainWindow).applyFilter(filter);
		} else {
			((ExpertMainWindow) mainWindow).applyFilter(filter);
		}
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void updateFilter(JTable table){
		TableModel model = table.getModel();
		ArrayList<String> columnNames = new ArrayList<String>();
		int nRow, nCol;
		nRow = model.getRowCount();
		nCol = model.getColumnCount();
		
		for(int i = 0; i < nCol; i++) {
			columnNames.add(model.getColumnName(i));
		}
		
		Set<String> artistSet, recordSet, genreSet, tempoSet;
		artistSet = new HashSet<String>();
		recordSet = new HashSet<String>();
		genreSet = new HashSet<String>();
		tempoSet = new HashSet<String>();
		
		int maxYear, minYear;
		maxYear = 0;
		minYear = 3000;
		
		for(int j = 0 ; j < nCol ; j++){
			for(int i = 0; i < nRow ; i++){
				switch(columnNames.get(j)){
				case "ศิลปิน":
					artistSet.add((String) model.getValueAt(i, j));
					break;
				case "สังกัด":
					recordSet.add((String) model.getValueAt(i, j));
					break;
				case "แนวเพลง":
					genreSet.add((String) model.getValueAt(i, j));
					break;
				case "จังหวะเพลง":
					tempoSet.add((String) model.getValueAt(i, j));
					break;
				case "ปีที่ปล่อย":
					int value = Integer.parseInt((String) model.getValueAt(i, j));
					if(value > maxYear) maxYear = value;
					if(value < minYear) minYear = value;
					break;
				}
			}
		}
		
		Vector artistVector = new Vector<>(artistSet);
		artistVector.add(0, "------");
		Vector recordVector = new Vector<>(recordSet);
		recordVector.add(0, "------");
		Vector genreVector = new Vector<>(genreSet);
		genreVector.add(0, "------");
		Vector tempoVector = new Vector<>(tempoSet);
		tempoVector.add(0, "------");
		
		
		artistModel = new DefaultComboBoxModel<>(artistVector);
		recordModel = new DefaultComboBoxModel<>(recordVector);
		genreModel = new DefaultComboBoxModel<>(genreVector);
		tempoModel = new DefaultComboBoxModel<>(tempoVector);
		
		comboArtist.setModel(artistModel);
		comboRecord.setModel(recordModel);
		comboGenre.setModel(genreModel);
		comboTempo.setModel(tempoModel);
		
		comboArtist.revalidate();
		comboRecord.revalidate();
		comboGenre.revalidate();
		comboTempo.revalidate();
		
		SpinnerModel from, to;
		from = new SpinnerNumberModel(minYear, minYear, maxYear, 1);
		to = new SpinnerNumberModel(maxYear, minYear, maxYear, 1);
		
		
		spinnerFromYear.setModel(from);
		spinnerToYear.setModel(to);
		JSpinner.NumberEditor fromEditor = new JSpinner.NumberEditor(spinnerFromYear, "#");
		JSpinner.NumberEditor toEditor = new JSpinner.NumberEditor(spinnerToYear, "#");
		spinnerFromYear.setEditor(fromEditor);
		spinnerToYear.setEditor(toEditor);
		
		spinnerFromYear.revalidate();
		spinnerToYear.revalidate();
	}

	private void initiate() {
		listener = new FilterListener();
		setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

		JLabel lblNewLabel = new JLabel("ศิลปิน");
		add(lblNewLabel);

		comboArtist = new JComboBox<String>();
		add(comboArtist);

		JLabel lblNewLabel_1 = new JLabel("สังกัด");
		add(lblNewLabel_1);

		comboRecord = new JComboBox<String>();
		add(comboRecord);

		JLabel lblNewLabel_2 = new JLabel("แนวเพลง");
		add(lblNewLabel_2);

		comboGenre = new JComboBox<String>();
		add(comboGenre);

		JLabel label = new JLabel("จังหวะเพลง");
		add(label);

		comboTempo = new JComboBox<String>();
		add(comboTempo);

		JLabel lblNewLabel_3 = new JLabel("ปีที่ปล่อย");
		add(lblNewLabel_3);

		spinnerFromYear = new JSpinner();
		add(spinnerFromYear);

		JLabel label_1 = new JLabel("-");
		add(label_1);

		spinnerToYear = new JSpinner();
		add(spinnerToYear);
		
		comboArtist.addActionListener(listener);
		comboGenre.addActionListener(listener);
		comboRecord.addActionListener(listener);
		comboTempo.addActionListener(listener);
		
		spinnerFromYear.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(checkboxUseFilter.isSelected()){
					if(mainWindow instanceof MainSearchMainWindow){
						((MainSearchMainWindow) mainWindow).resetTable(2);
					} else {
						((ExpertMainWindow) mainWindow).resetTable(2);
					}
					performApplyFilter();
				}
			}
		});
		
		spinnerToYear.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(checkboxUseFilter.isSelected()){
					if(mainWindow instanceof MainSearchMainWindow){
						((MainSearchMainWindow) mainWindow).resetTable(2);
					} else {
						((ExpertMainWindow) mainWindow).resetTable(2);
					}
					performApplyFilter();
				}
				
			}
		});
	}

	class FilterListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(checkboxUseFilter.isSelected()){
				if(mainWindow instanceof MainSearchMainWindow){
					((MainSearchMainWindow) mainWindow).resetTable(2);
				} else {
					((ExpertMainWindow) mainWindow).resetTable(2);
				}
				performApplyFilter();
			}
		}
		
	}
}
