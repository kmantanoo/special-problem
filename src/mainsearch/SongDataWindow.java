package mainsearch;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import java.awt.Insets;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SongDataWindow extends JFrame implements WindowListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JTextField tfSongName;
	private static JTextField tfArtist;
	private static JTextField tfGenre;
	private static JTextField tfTempo;
	private static JTextField tfFeeling;
	private static JTextField tfRecords;
	private static JTextField tfYearReleased;
	private static JTextField tfLyric;
	private static JTextField tfMelody;
	private static SongDataWindow instance;
	
	private SongDataWindow(ArrayList<String> datas){
		
		setTitle(datas.get(0));
		setBounds(650, 150, 306, 293);
		initialize();
		for(int i = 1; i < datas.size()+1; i++){
    		String data = (String) datas.get(i-1);
    		switch(i){
    		case 1:
    			setTfSongName(data);
    			break;
    		case 2:
    			setTfArtist(data);
    			break;
    		case 3:
    			setTfRecords(data);
    			break;
    		case 4:
    			setTfFeeling(data);
    			break;
    		case 5:
    			setTfGenre(data);
    			break;
    		case 6:
    			setTfTempo(data);
    			break;
    		case 7:
    			setTfYearReleased(data);
    			break;
    		case 8:
    			setTfLyric(data);
    			break;
    		case 9:
    			setTfMelody(data);
    			break;
    		}
    	}
		setVisible(true);
	}
	
	public static void updateData(ArrayList<String> datas){
		instance = null;
		instantiate(datas);
	}
	
	public static void instantiate(ArrayList<String> datas){
		if(instance == null) instance = new SongDataWindow(datas);
	}
	
	private void initialize() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblNewLabel_1 = new JLabel("\u0E40\u0E1E\u0E25\u0E07");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		tfSongName = new JTextField();
		tfSongName.setEditable(false);
		GridBagConstraints gbc_tfSongName = new GridBagConstraints();
		gbc_tfSongName.anchor = GridBagConstraints.NORTH;
		gbc_tfSongName.gridwidth = 4;
		gbc_tfSongName.insets = new Insets(0, 0, 5, 5);
		gbc_tfSongName.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfSongName.gridx = 1;
		gbc_tfSongName.gridy = 0;
		getContentPane().add(tfSongName, gbc_tfSongName);
		tfSongName.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("\u0E28\u0E34\u0E25\u0E1B\u0E34\u0E19");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 1;
		getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		tfArtist = new JTextField();
		tfArtist.setEditable(false);
		tfArtist.setColumns(10);
		GridBagConstraints gbc_tfArtist = new GridBagConstraints();
		gbc_tfArtist.gridwidth = 3;
		gbc_tfArtist.insets = new Insets(0, 0, 5, 5);
		gbc_tfArtist.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfArtist.gridx = 1;
		gbc_tfArtist.gridy = 1;
		getContentPane().add(tfArtist, gbc_tfArtist);
		
		JLabel lblNewLabel_3 = new JLabel("\u0E41\u0E19\u0E27\u0E40\u0E1E\u0E25\u0E07");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 2;
		getContentPane().add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		tfGenre = new JTextField();
		tfGenre.setEditable(false);
		tfGenre.setColumns(10);
		GridBagConstraints gbc_tfGenre = new GridBagConstraints();
		gbc_tfGenre.gridwidth = 2;
		gbc_tfGenre.insets = new Insets(0, 0, 5, 5);
		gbc_tfGenre.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfGenre.gridx = 1;
		gbc_tfGenre.gridy = 2;
		getContentPane().add(tfGenre, gbc_tfGenre);
		
		JLabel lblNewLabel_4 = new JLabel("\u0E08\u0E31\u0E07\u0E2B\u0E27\u0E30\u0E40\u0E1E\u0E25\u0E07");
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.gridx = 0;
		gbc_lblNewLabel_4.gridy = 3;
		getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);
		
		tfTempo = new JTextField();
		tfTempo.setEditable(false);
		tfTempo.setColumns(10);
		GridBagConstraints gbc_tfTempo = new GridBagConstraints();
		gbc_tfTempo.gridwidth = 2;
		gbc_tfTempo.insets = new Insets(0, 0, 5, 5);
		gbc_tfTempo.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfTempo.gridx = 1;
		gbc_tfTempo.gridy = 3;
		getContentPane().add(tfTempo, gbc_tfTempo);
		
		JLabel lblNewLabel_6 = new JLabel("\u0E2D\u0E32\u0E23\u0E21\u0E13\u0E4C\u0E40\u0E1E\u0E25\u0E07");
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_6.gridx = 0;
		gbc_lblNewLabel_6.gridy = 4;
		getContentPane().add(lblNewLabel_6, gbc_lblNewLabel_6);
		
		tfFeeling = new JTextField();
		tfFeeling.setEditable(false);
		tfFeeling.setColumns(10);
		GridBagConstraints gbc_tfFeeling = new GridBagConstraints();
		gbc_tfFeeling.gridwidth = 3;
		gbc_tfFeeling.insets = new Insets(0, 0, 5, 5);
		gbc_tfFeeling.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfFeeling.gridx = 1;
		gbc_tfFeeling.gridy = 4;
		getContentPane().add(tfFeeling, gbc_tfFeeling);
		
		JLabel lblNewLabel = new JLabel("\u0E2A\u0E31\u0E07\u0E01\u0E31\u0E14");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 5;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		tfRecords = new JTextField();
		tfRecords.setEditable(false);
		tfRecords.setColumns(10);
		GridBagConstraints gbc_tfRecords = new GridBagConstraints();
		gbc_tfRecords.gridwidth = 2;
		gbc_tfRecords.insets = new Insets(0, 0, 5, 5);
		gbc_tfRecords.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfRecords.gridx = 1;
		gbc_tfRecords.gridy = 5;
		getContentPane().add(tfRecords, gbc_tfRecords);
		
		JLabel lblNewLabel_7 = new JLabel("\u0E1B\u0E35\u0E17\u0E35\u0E48\u0E1B\u0E25\u0E48\u0E2D\u0E22");
		GridBagConstraints gbc_lblNewLabel_7 = new GridBagConstraints();
		gbc_lblNewLabel_7.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_7.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_7.gridx = 0;
		gbc_lblNewLabel_7.gridy = 6;
		getContentPane().add(lblNewLabel_7, gbc_lblNewLabel_7);
		
		tfYearReleased = new JTextField();
		tfYearReleased.setEditable(false);
		tfYearReleased.setColumns(10);
		GridBagConstraints gbc_tfYearReleased = new GridBagConstraints();
		gbc_tfYearReleased.gridwidth = 2;
		gbc_tfYearReleased.insets = new Insets(0, 0, 5, 5);
		gbc_tfYearReleased.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfYearReleased.gridx = 1;
		gbc_tfYearReleased.gridy = 6;
		getContentPane().add(tfYearReleased, gbc_tfYearReleased);
		
		JLabel lblNewLabel_8 = new JLabel("\u0E1C\u0E39\u0E49\u0E41\u0E15\u0E48\u0E07\u0E40\u0E19\u0E37\u0E49\u0E2D\u0E23\u0E49\u0E2D\u0E07");
		GridBagConstraints gbc_lblNewLabel_8 = new GridBagConstraints();
		gbc_lblNewLabel_8.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_8.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_8.gridx = 0;
		gbc_lblNewLabel_8.gridy = 7;
		getContentPane().add(lblNewLabel_8, gbc_lblNewLabel_8);
		
		tfLyric = new JTextField();
		tfLyric.setEditable(false);
		tfLyric.setColumns(10);
		GridBagConstraints gbc_tfLyric = new GridBagConstraints();
		gbc_tfLyric.gridwidth = 4;
		gbc_tfLyric.insets = new Insets(0, 0, 5, 5);
		gbc_tfLyric.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfLyric.gridx = 1;
		gbc_tfLyric.gridy = 7;
		getContentPane().add(tfLyric, gbc_tfLyric);
		
		JLabel lblNewLabel_9 = new JLabel("\u0E1C\u0E39\u0E49\u0E41\u0E15\u0E48\u0E07\u0E17\u0E33\u0E19\u0E2D\u0E07");
		GridBagConstraints gbc_lblNewLabel_9 = new GridBagConstraints();
		gbc_lblNewLabel_9.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_9.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_9.gridx = 0;
		gbc_lblNewLabel_9.gridy = 8;
		getContentPane().add(lblNewLabel_9, gbc_lblNewLabel_9);
		
		tfMelody = new JTextField();
		tfMelody.setEditable(false);
		tfMelody.setColumns(10);
		GridBagConstraints gbc_tfMelody = new GridBagConstraints();
		gbc_tfMelody.gridwidth = 4;
		gbc_tfMelody.insets = new Insets(0, 0, 5, 5);
		gbc_tfMelody.fill = GridBagConstraints.HORIZONTAL;
		gbc_tfMelody.gridx = 1;
		gbc_tfMelody.gridy = 8;
		getContentPane().add(tfMelody, gbc_tfMelody);
		
		
		
		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				instance = null;
			}
		});
		GridBagConstraints gbc_btnOK = new GridBagConstraints();
		gbc_btnOK.anchor = GridBagConstraints.NORTH;
		gbc_btnOK.insets = new Insets(0, 0, 0, 5);
		gbc_btnOK.gridx = 2;
		gbc_btnOK.gridy = 9;
		getContentPane().add(btnOK, gbc_btnOK);
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		dispose();
		instance = null;
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public static void setText(JTextField tf, String data){
		if(haveInstance()) tf.setText(data);
	}

	private static void setTfSongName(String data) {
		tfSongName.setText(data);
	}

	private static void setTfArtist(String data) {
		tfArtist.setText(data);
	}

	private static void setTfGenre(String data) {
		tfGenre.setText(data);
	}

	private static void setTfTempo(String data) {
		tfTempo.setText(data);
	}

	private static void setTfFeeling(String data) {
		tfFeeling.setText(data);
	}

	private static void setTfRecords(String data) {
		tfRecords.setText(data);
	}

	private static void setTfYearReleased(String data) {
		tfYearReleased.setText(data);
	}

	private static void setTfLyric(String data) {
		tfLyric.setText(data);
	}

	private static void setTfMelody(String data) {
		tfMelody.setText(data);
	}

	public static boolean haveInstance() {
		return instance!=null;
	}
	

	
}
