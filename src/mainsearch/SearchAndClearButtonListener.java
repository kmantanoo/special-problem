package mainsearch;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.persistence.TypedQuery;
import javax.swing.JPanel;

import entity.Song;
import expert.ExpertMainSearchPanel;
import service.GeneralQueryService;
import service.RecommenderService;
import service.TableDataService;

public class SearchAndClearButtonListener implements ActionListener {

	private CardsPanel cards;
	private JPanel view;
	
	public SearchAndClearButtonListener(CardsPanel cards, JPanel view) {
		this.cards = cards;
		this.view = view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()){
		case "ค้นหา":
			performSearch();
			break;
		case "ล้างการค้นหา":
			performClear();
			break;
		}
	}

	@SuppressWarnings("unchecked")
	private void performSearch() {
		/***
		 * if search mode is used attribute of the song
		 */
		Vector<Vector<String>> data=null;
		if(cards.getSelectedItem().size()==2){
			String mode, value;
			mode = cards.getSelectedItem().get(0);
			value = cards.getSelectedItem().get(1);
			
			
			TypedQuery<Song> result=null;
			String namedQuery, parameterName, resultNamedQuery, resultParameterName;
			namedQuery = parameterName = resultNamedQuery = resultParameterName = "";
			Object valueObject, foundObject;
			switch(mode){
			case "ศิลปิน":
				namedQuery = "Artist.findIdByName";
				parameterName = "artist_name";
				resultNamedQuery = "Song.findByArtist";
				resultParameterName = "artistid";
				break;
			case "สังกัด":
				namedQuery = "Record.findIDByName";
				parameterName = "record_name";
				resultNamedQuery = "Song.findByRecord";
				resultParameterName = "recordid";
				break;
			case "อารมณ์เพลง":
				namedQuery = "Songmood.findIDByLabel";
				parameterName = "song_mood_label";
				resultNamedQuery = "Song.findBySongmood";
				resultParameterName = "moodid";
				break;
			case "แนวเพลง":
				namedQuery = "Genre.findIDByLabel";
				parameterName = "genre_label";
				resultNamedQuery = "Song.findByGenre";
				resultParameterName = "genreid";
				break;
			case "จังหวะเพลง":
				namedQuery = "Tempo.findIDByLabel";
				parameterName = "tempo_label";
				resultNamedQuery = "Song.findByTempo";
				resultParameterName = "tempoid";
				break;
			}
			valueObject = value;
			foundObject = GeneralQueryService.find(namedQuery).setParameter(parameterName, valueObject).getSingleResult();
			result = (TypedQuery<Song>) GeneralQueryService.find(resultNamedQuery).setParameter(resultParameterName, foundObject);
			
			data = TableDataService.loadTableData(result.getResultList());
		} else {
			/***
			 * If search mode is search by event
			 */
			String event = cards.getSelectedItem().get(0);
			try {
				BufferedReader recFile = new BufferedReader(new FileReader(new File(RecommenderService.path()+"temp-"+RecommenderService.fileName + event + RecommenderService.fileType)));
				Vector<Integer> ids = new Vector<>();
				Vector<Song> songs = new Vector<>();
				String line;
				while((line=recFile.readLine()) != null){
					ids.add(Integer.parseInt(line));
				}
				for(Integer i:ids){
					TypedQuery<Song> song = GeneralQueryService.find("Song.findByID", Song.class).setParameter("sid", i);
					songs.add(song.getSingleResult());
				}
				
				data = TableDataService.loadTableData(songs);
				recFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(view instanceof MainSearchPanel){
			((MainSearchPanel)view).updateTable(data, true, 1);
		} else {
			((ExpertMainSearchPanel) view).updateTable(data, true, 1);
		}
		
	}

	private void performClear() {
		if(view instanceof MainSearchPanel){
			((MainSearchPanel) view).resetTable(1);
			((MainSearchPanel) view).updateFilter();
		} else {
			((ExpertMainSearchPanel) view).resetTable(1);
			((ExpertMainSearchPanel) view).updateFilter();
		}
	}

}
