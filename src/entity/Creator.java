package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the creator database table.
 * 
 */
@Entity
@NamedQuery(name="Creator.findAll", query="SELECT c FROM Creator c")
public class Creator implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@TableGenerator(name = "CREATOR_GEN",
			table = "sequence", 
			pkColumnName = "SEQ_NAME", 
			valueColumnName = "SEQ_COUNT", 
			pkColumnValue = "CREATOR", 
			allocationSize = 1)
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="CREATOR_GEN")
	private int cid;
	
	@OneToOne
	@JoinColumn(name="aid")
	private Artist aid;

	private String lyricist;

	private String melodywriter;

	@OneToOne
	@JoinColumn(name="rid")
	private Record rid;

	public Creator() {
	}

	public int getCid() {
		return this.cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public Artist getArtist() {
		return this.aid;
	}

	public void setArtist(Artist aid) {
		this.aid = aid;
	}

	public String getLyricist() {
		return this.lyricist;
	}

	public void setLyricist(String lyricist) {
		this.lyricist = lyricist;
	}

	public String getMelodywriter() {
		return this.melodywriter;
	}

	public void setMelodywriter(String melodywriter) {
		this.melodywriter = melodywriter;
	}

	public Record getRecord() {
		return this.rid;
	}

	public void setRecord(Record rid) {
		this.rid = rid;
	}

}