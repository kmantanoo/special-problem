package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the record database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Record.findAll", query="SELECT r FROM Record r"),
	@NamedQuery(name="Record.findIDByName", query="SELECT r FROM Record r WHERE r.rName=:record_name")
})

public class Record implements Serializable {
	private static final long serialVersionUID = 1L;

	@TableGenerator(name = "RECORD_GEN",
			table = "sequence", 
			pkColumnName = "SEQ_NAME", 
			valueColumnName = "SEQ_COUNT", 
			pkColumnValue = "RECORD", 
			allocationSize = 1)
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="RECORD_GEN")
	@Column(name="r_id")
	private int rId;

	@Column(name="r_name")
	private String rName;

	public Record() {
	}

	public int getRId() {
		return this.rId;
	}

	public void setRId(int rId) {
		this.rId = rId;
	}

	public String getRName() {
		return this.rName;
	}

	public void setRName(String rName) {
		this.rName = rName;
	}
	
	@Override
	public String toString(){
		return getRName();
	}

}