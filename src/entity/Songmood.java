package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the songmood database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Songmood.findAll", query="SELECT s FROM Songmood s"),
	@NamedQuery(name="Songmood.findIDByLabel", query="SELECT sm FROM Songmood sm WHERE sm.moodLabel=:song_mood_label")
})
public class Songmood implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="mood_id")
	private int moodId;

	@Column(name="mood_label")
	private String moodLabel;

	public Songmood() {
	}

	public int getMoodId() {
		return this.moodId;
	}

	public void setMoodId(int moodId) {
		this.moodId = moodId;
	}

	public String getMoodLabel() {
		return this.moodLabel;
	}

	public void setMoodLabel(String moodLabel) {
		this.moodLabel = moodLabel;
	}
	
	@Override
	public String toString(){
		return getMoodLabel();
	}

}