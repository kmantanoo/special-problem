package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the song database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Song.findAll", query="SELECT s FROM Song s"),
	@NamedQuery(name="Song.findByArtist", query="SELECT s FROM Song s WHERE s.cid IN (SELECT c FROM Creator c WHERE c.aid=:artistid)"),
	@NamedQuery(name="Song.findByRecord", query="SELECT s FROM Song s WHERE s.cid IN (SELECT c FROM Creator c WHERE c.rid=:recordid)"),
	@NamedQuery(name="Song.findBySongmood", query="SELECT s FROM Song s WHERE s.songmood=:moodid"),
	@NamedQuery(name="Song.findByTempo", query="SELECT s FROM Song s WHERE s.tempo=:tempoid"),
	@NamedQuery(name="Song.findByGenre", query="SELECT s FROM Song s WHERE s.genre=:genreid"),
	@NamedQuery(name="Song.findByID", query="SELECT s FROM Song s WHERE s.sid=:sid")
})
public class Song implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@TableGenerator(name = "SONG_GEN",
			table = "sequence", 
			pkColumnName = "SEQ_NAME", 
			valueColumnName = "SEQ_COUNT", 
			pkColumnValue = "SONG", 
			allocationSize = 1)
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="SONG_GEN")
	private int sid;

	@OneToOne
	@JoinColumn(name="cid")
	private Creator cid;

	@OneToOne
	@JoinColumn(name="genre")
	private Genre genre;

	private int releaseyear;

	@OneToOne
	@JoinColumn(name="songmood")
	private Songmood songmood;

	private String songname;

	@OneToOne
	@JoinColumn(name="tempo")
	private Tempo tempo;

	public Song() {
	}

	public int getSid() {
		return this.sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public Creator getCreator() {
		return this.cid;
	}

	public void setCreator(Creator cid) {
		this.cid = cid;
	}

	public Genre getGenre() {
		return this.genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public int getReleaseyear() {
		return this.releaseyear;
	}

	public void setReleaseyear(int releaseyear) {
		this.releaseyear = releaseyear;
	}

	public Songmood getSongmood() {
		return this.songmood;
	}

	public void setSongmood(Songmood songmood) {
		this.songmood = songmood;
	}

	public String getSongname() {
		return this.songname;
	}

	public void setSongname(String songname) {
		this.songname = songname;
	}

	public Tempo getTempo() {
		return this.tempo;
	}

	public void setTempo(Tempo tempo) {
		this.tempo = tempo;
	}
	
	@Override
	public String toString(){
		return getSongname();
	}

}