package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tempo database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Tempo.findAll", query="SELECT t FROM Tempo t"),
	@NamedQuery(name="Tempo.findIDByLabel", query="SELECT t FROM Tempo t WHERE t.tLabel=:tempo_label")
})
public class Tempo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="t_id")
	private int tId;

	@Column(name="t_label")
	private String tLabel;

	public Tempo() {
	}

	public int getTId() {
		return this.tId;
	}

	public void setTId(int tId) {
		this.tId = tId;
	}

	public String getTLabel() {
		return this.tLabel;
	}

	public void setTLabel(String tLabel) {
		this.tLabel = tLabel;
	}
	
	@Override
	public String toString(){
		return getTLabel();
	}

}