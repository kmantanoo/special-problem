package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the genre database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Genre.findAll", query="SELECT g FROM Genre g"),
	@NamedQuery(name="Genre.findIDByLabel", query="SELECT g FROM Genre g WHERE g.gLabel=:genre_label")
})

public class Genre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="g_id")
	private int gId;

	@Column(name="g_label")
	private String gLabel;

	public Genre() {
	}

	public int getGId() {
		return this.gId;
	}

	public void setGId(int gId) {
		this.gId = gId;
	}

	public String getGLabel() {
		return this.gLabel;
	}

	public void setGLabel(String gLabel) {
		this.gLabel = gLabel;
	}
	
	@Override
	public String toString(){
		return getGLabel();
	}

}