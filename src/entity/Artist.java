package entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the artist database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Artist.findAll", query="SELECT a FROM Artist a"),
	@NamedQuery(name="Artist.findIdByName", query="SELECT a FROM Artist a WHERE a.aName=:artist_name")
})
public class Artist implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@TableGenerator(name = "ARTIST_GEN",
			table = "sequence", 
			pkColumnName = "SEQ_NAME", 
			valueColumnName = "SEQ_COUNT", 
			pkColumnValue = "ARTIST", 
			allocationSize = 1)
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE, generator="ARTIST_GEN")
	@Column(name="a_id")
	private int aId;

	@Column(name="a_name")
	private String aName;

	public Artist() {
	}

	public int getAId() {
		return this.aId;
	}

	public void setAId(int aId) {
		this.aId = aId;
	}

	public String getAName() {
		return this.aName;
	}

	public void setAName(String aName) {
		this.aName = aName;
	}
	
	@Override
	public String toString(){
		return getAName();
	}

}